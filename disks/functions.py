import numpy as np
import pandas as pd

def truncation_radius():
    pass

def data_papaloizou_pringle77():
    """
    Function to return the following:

    dataframe of their table 1:
    q = m2 / m1 = Mdonor/Maccretor
    alpha_s = truncation radius (expressed in units of separation)
    alpha_L = mean roche lobe radius (expressed in units of separation)
    
    alpha is expressed as r/R, indeed separation normalised.
    the disc is orbitting around M1, so the 

    what are the downsides of this paper? well its a linearized approach afaik.. it might be that some non-linearities fuck things up.

    """
    
    q_arr = np.array([0.2, 0.4, 0.6, 0.8, 1.0, 2.0, 4.0, 6.0, 8.0, 10.0])
    alpha_s = np.array([0.45, 0.41, 0.37, 0.35, 0.33, 0.28, 0.23, 0.21, 0.19, 0.18])
    alpha_L = np.array([0.52, 0.46, 0.42, 0.40, 0.38, 0.32, 0.26, 0.23, 0.22, 0.20])

    return pd.DataFrame(np.transpose(np.array([q_arr, alpha_s, alpha_L])), columns=['q', 'alpha_s', 'alpha_L'])