"""
Example of multiprocessing via a queue based on functions

setproctitle is used for the proces title
"""

import os
import time
import pickle
import multiprocessing

import setproctitle


def convolution_job_worker(job_queue, worker_ID, configuration):
    """
    Function that handles running the job
    """

    # Set name worker
    setproctitle.setproctitle("Multiprocessing worker process {}".format(worker_ID))

    # Get items from the job_queue
    for job_dict in iter(job_queue.get, "STOP"):
        #########
        # Stopping or working
        if job_dict == "STOP":
            return None

        ##########
        # Set up output dict
        output_dict = {}

        ##########
        # doe iets met de job dict, paas t naar een normale functie, maak een class obj en laat die iets berekenen, idk
        print("Worker {} handling job {}".format(worker_ID, job_dict))

        # Write to file, or do something else
        with open(
            os.path.join(configuration["output_dir"], "{}.txt".format(worker_ID)), "a"
        ) as f:
            f.write(str(job_dict))

    return None


def multiprocessing_queue_filler(job_queue, num_cores, configuration, job_iterable):
    """
    Function to handle filling the queue for the multiprocessing
    """

    # Continuously fill the queue
    for job_i, job_dict in enumerate(job_iterable):

        # Put job in queue
        job_queue.put(job_dict)

    # Signal stop to workers
    for _ in range(num_cores):
        job_queue.put("STOP")


def multiprocessing_main(
    configuration,
):
    """
    Main process to handle the multiprocessing
    """

    ###################
    # Run the convolution through multiprocessing

    start = time.time()

    # Set process name
    setproctitle.setproctitle("Convolution parent process")

    # Set up the manager object that can share info between processes NOTE: you can add a result queue here
    manager = multiprocessing.Manager()
    job_queue = manager.Queue(configuration["max_job_queue_size"])

    # Create process instances
    processes = []
    for worker_ID in range(configuration["num_cores"]):
        processes.append(
            multiprocessing.Process(
                target=convolution_job_worker,
                args=(job_queue, worker_ID, configuration),
            )
        )

    # Activate the processes
    for p in processes:
        p.start()

    # Set up the system_queue
    multiprocessing_queue_filler(
        job_queue=job_queue,
        num_cores=configuration["num_cores"],
        configuration=configuration,
        job_iterable=[{"ID": i} for i in range(1000)],
    )

    # Join the processes
    for p in processes:
        p.join()
    print(
        "Finished multiprocessing all the tasks. This took {}".format(
            time.time() - start
        )
    )


if __name__ == "__main__":

    configuration = {
        "output_dir": os.path.join(os.getcwd(), "results"),
        "num_cores": 4,
        "max_job_queue_size": 10,
    }

    os.makedirs(configuration["output_dir"], exist_ok=True)

    multiprocessing_main(configuration)
