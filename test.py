import matplotlib.pyplot as plt
import numpy as np
from david_phd_functions.disks.functions import data_papaloizou_pringle77


ding = data_papaloizou_pringle77()
ding['ratio'] = ding['alpha_s']/ding['alpha_L']
print(ding)

# plt.plot(ding['q'], ding['alpha_s'])
# plt.plot(ding['q'], ding['alpha_L'])
# plt.xscale('log')
# plt.show()

# quit()


from david_phd_functions.masstransfer.functions import circularisation_radius, rochelobe_radius_accretor, rochelobe_radius_donor, b1


mass_donor = 1
mass_accretor = 1 


# print(help(circularisation_radius))
print(b1(mass_donor, mass_accretor))
print(circularisation_radius(mass_donor, mass_accretor))
print(rochelobe_radius_accretor(mass_donor, mass_accretor))
print(rochelobe_radius_donor(mass_donor, mass_accretor))


masses_donor = np.arange(0.1, 10, 0.1)
masses_accretor = np.arange(0.1, 10, 0.1)

masses = np.array([masses_donor, np.flip(masses_accretor)])
mass_combos = zip(masses_donor, np.flip(masses_accretor))



b1_vals, circularisation_radii, RL_radii_accretor, RL_radii_donor = [],[],[],[]

for mass_combo in mass_combos:
    mass_donor = mass_combo[0]
    mass_accretor = mass_combo[1]

    b1_vals.append(b1(mass_donor, mass_accretor))
