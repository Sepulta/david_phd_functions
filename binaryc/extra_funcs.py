import json

def print_defaults(settings_file):
    """
    Function to print out the default values. The input parameters will be printed first.
    """

    with open(settings_file, 'r') as f:
        settings = json.loads(f.read())


    print("===============================")
    print("Custom values: (overriding defaults)")
    for el in settings['population_settings']['bse_options']:
        print("\t{}: {}".format(el, settings['population_settings']['bse_options'][el]))

    print("binary_c defaults")
    for el in sorted(settings['binary_c_defaults']):
        if not el in set(settings['population_settings']['bse_options'].keys()):
            print("\t{}: {}".format(el, settings['binary_c_defaults'][el]))