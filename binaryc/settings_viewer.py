"""
Function to show the used BSE settings of a simulation
"""

import json

file = '/home/david/projects/binary_c_root/results/GRAV_WAVES/LOW_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_ON/Z0.0001/simulation_20211010_141601_settings.json'
parameter_list = [
    'BH_prescription',
    'wind_mass_loss',
    'RLOF_interpolation_method',
    'PPISN_prescription',
    'alpha_ce',
    'Bondi_Hoyle_accretion_factor',
    'donor_limit_dynamical_multiplier',
    'donor_limit_envelope_multiplier',
    'donor_limit_thermal_multiplier',
    'effective_metallicity',
    'gravitational_radiation_model',
    'lambda_ce',
    'merger_angular_momentum_factor',
    'merger_mass_loss_fraction',
    'nonconservative_angmom_gamma',
    'overspin_algorithm',
    'PPISN_additional_massloss',
    'RLOF_angular_momentum_transfer_model',
    'RLOF_method',
]


qcrit_list = [
    'qcrit_BH',
    'qcrit_CHeB',
    'qcrit_COWD',
    'qcrit_degenerate_BH',
    'qcrit_degenerate_CHeB',
    'qcrit_degenerate_COWD',
    'qcrit_degenerate_EAGB',
    'qcrit_degenerate_GB',
    'qcrit_degenerate_HeGB',
    'qcrit_degenerate_HeHG',
    'qcrit_degenerate_HeMS',
    'qcrit_degenerate_HeWD',
    'qcrit_degenerate_HG',
    'qcrit_degenerate_LMMS',
    'qcrit_degenerate_MS',
    'qcrit_degenerate_NS',
    'qcrit_degenerate_ONeWD',
    'qcrit_degenerate_TPAGB',
    'qcrit_EAGB',
    'qcrit_GB',
    'qcrit_HeGB',
    'qcrit_HeHG',
    'qcrit_HeMS',
    'qcrit_HeWD',
    'qcrit_HG',
    'qcrit_LMMS',
    'qcrit_MS',
    'qcrit_NS',
    'qcrit_ONeWD',
    'qcrit_TPAGB',



]

def print_parameter_values(file, parameter_list, print_description=True):
    """
    Function to print the parameter values used in the simulation
    """

    used_values = {}
    help_info = {}

    # Open file
    with open(file, 'r') as f:
        settings = json.loads(f.read())

    # Look for the values of the parameters
    for parameter in parameter_list:
        if parameter in settings['population_settings']['bse_options'].keys():
            used_values[parameter] = settings['population_settings']['bse_options'][parameter]
        elif parameter in settings['binary_c_defaults'].keys():
            used_values[parameter] = settings['binary_c_defaults'][parameter]

    # Look for the help of the parameters
    help_all_info = settings['binary_c_help_all']
    for parameter in parameter_list:
        for section in help_all_info.keys():
            for section_parameter in help_all_info[section]['parameters'].keys():
                if parameter == section_parameter:
                    help_info[parameter] = help_all_info[section]['parameters'][section_parameter]

    # Print the info
    for parameter in parameter_list:
        print("Parameter name:\n\t{}".format(parameter))
        print("Used value:\n\t{}".format(used_values[parameter]))
        if print_description:
            print("{}".format(help_info[parameter]['description']))
        print("\n")

def print_available_parameter_names(file):
    """
    Function to print the available parameter names
    """

    # Open file
    with open(file, 'r') as f:
        settings = json.loads(f.read())

    print("The available parameters in the current simulation file are")
    for parameter in sorted(settings['binary_c_defaults'].keys(), key=str.casefold):
        print("\t{}".format(parameter))


print_parameter_values(file, qcrit_list)
