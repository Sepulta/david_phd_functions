import time
import os
import h5py
import json
import pandas as pd


def process_RLOF_episodes_stable_initial_final(DATA_DIR):
    """
    Function that processes all the RLOF episode files.

    Takes the first and last timestep of the RLOF and makes a summary.
    
    Writes the hdf5 file to the DATA_DIR
    """

    processing_start = time.time()

    all_files = os.listdir(DATA_DIR)
    datafiles = [el for el in all_files if el.endswith('.dat')]
    settings_files = [el for el in all_files if el.endswith('_settings.json')]

    unstable_MT_count = 0
    unstable_MT_data = []
    initially_stable_MT_count = 0
    initially_stable_MT_data = []

    wrong_disc_line_files = 0

    print("{} contains {} files, {} datafiles".format(DATA_DIR, len(all_files), len(datafiles)))

    for datafile in datafiles:
        with open(os.path.join(DATA_DIR, datafile), 'r') as f:
            data = f.readlines()

            if len(data)==2:
                unstable_MT_count += 1 
                pass
            elif len(data)>2:
                initially_stable_MT_count += 1 

                # 
                header = data[0].split()
                RLOF_data = data[1:]
                first_timestep = RLOF_data[0].split()
                second_timestep = RLOF_data[1].split()
                last_timestep = RLOF_data[-1].split()


                # Get indices of interesting information
                # System common info
                zams_mass_1_index = header.index('zams_mass_1')
                zams_mass_2_index = header.index('zams_mass_2')
                zams_period_index = header.index('zams_period')
                metallicity_index = header.index('metallicity')
                probability_index = header.index('probability')
                rlof_counter_index = header.index('rlof_counter')
                random_seed_index = header.index('random_seed_extra')


                # RLof specific info
                time_index = header.index('time')
                period_index = header.index('period')
                mass_1_index = header.index('mass_1')
                mass_2_index = header.index('mass_2')
                radius_1_index = header.index('radius_1')
                radius_2_index = header.index('radius_2')
                stellar_type_1_index = header.index('stellar_type_1')
                stellar_type_2_index = header.index('stellar_type_2')
                disk_index           = header.index('disk')
                rlof_stability_index  = header.index('rlof_stability')
                rlof_type_index = header.index('rlof_type')
                accretor_index = header.index('accretor')
                donor_index = header.index('donor')
                separation_index = header.index('separation')

                ## Calculate or select the interesting stuff
                # system common stuff
                zams_mass_1 = first_timestep[zams_mass_1_index]
                zams_mass_2 = first_timestep[zams_mass_2_index]
                zams_period = first_timestep[zams_period_index]
                metallicity = first_timestep[metallicity_index]
                probability = first_timestep[probability_index]
                rlof_counter = first_timestep[rlof_counter_index]
                random_seed = first_timestep[random_seed_index]

                # RLOF specific stuff
                time_start = first_timestep[time_index]
                time_end = last_timestep[time_index]

                initial_period = first_timestep[period_index]
                final_period = last_timestep[period_index]

                initial_mass_1 = first_timestep[mass_1_index]
                final_mass_1 = last_timestep[mass_1_index]
                initial_mass_2 = first_timestep[mass_2_index]
                final_mass_2 = last_timestep[mass_2_index]

                initial_radius_1 = first_timestep[radius_1_index]
                final_radius_1 = last_timestep[radius_1_index]
                initial_radius_2 = first_timestep[radius_2_index]
                final_radius_2 = last_timestep[radius_2_index]

                initial_stellar_type_1 = first_timestep[stellar_type_1_index]
                final_stellar_type_1 = last_timestep[stellar_type_1_index]
                initial_stellar_type_2 = first_timestep[stellar_type_2_index]
                final_stellar_type_2 = last_timestep[stellar_type_2_index]

                initial_disk    = first_timestep[disk_index]
                final_disk    = last_timestep[disk_index]

                initial_stability = first_timestep[rlof_stability_index]
                final_stability = last_timestep[rlof_stability_index]

                initial_type = first_timestep[rlof_type_index]
                final_type = last_timestep[rlof_type_index]

                initial_accretor = first_timestep[accretor_index]
                final_accretor = last_timestep[accretor_index]
                initial_donor = first_timestep[donor_index]
                final_donor = last_timestep[donor_index]

                initial_separation = first_timestep[separation_index]
                final_separation = last_timestep[separation_index]

                ### Add to new list/data
                ## Info common to the system
                # Add this header only the first time
                if (initially_stable_MT_count==1):
                    new_header = []

                    new_header.append('zams_mass_1')
                    new_header.append('zams_mass_2')
                    new_header.append('zams_period')
                    new_header.append('metallicity')
                    new_header.append('probability')
                    new_header.append('rlof_counter')
                    new_header.append('random_seed')
                    new_header.append('filename')

                    new_header.append('time_start')
                    new_header.append('time_end')

                    new_header.append('initial_period')
                    new_header.append('final_period')

                    new_header.append('initial_mass_1')
                    new_header.append('final_mass_1')
                    new_header.append('initial_mass_2')
                    new_header.append('final_mass_2')

                    new_header.append('initial_radius_1')
                    new_header.append('final_radius_1')
                    new_header.append('initial_radius_2')
                    new_header.append('final_radius_2')

                    new_header.append('initial_stellar_type_1')
                    new_header.append('final_stellar_type_1')
                    new_header.append('initial_stellar_type_2')
                    new_header.append('final_stellar_type_2')

                    new_header.append('initial_disk')
                    new_header.append('final_disk')

                    new_header.append('initial_stability')
                    new_header.append('final_stability')

                    new_header.append('initial_type')
                    new_header.append('final_type')

                    new_header.append('initial_accretor')
                    new_header.append('final_accretor')
                    new_header.append('initial_donor')
                    new_header.append('final_donor')

                    new_header.append('initial_separation')
                    new_header.append('final_separation')

                    print("Old header:\n\t{}".format(header))
                    print("New header:\n\t{}".format(new_header))


                new_data = []

                new_data.append(zams_mass_1)
                new_data.append(zams_mass_2)
                new_data.append(zams_period)
                new_data.append(metallicity)
                new_data.append(probability)
                new_data.append(rlof_counter)
                new_data.append(random_seed)
                new_data.append(os.path.join(DATA_DIR, datafile))

                new_data.append(time_start)
                new_data.append(time_end)

                new_data.append(initial_period)
                new_data.append(final_period)
                
                new_data.append(initial_mass_1)
                new_data.append(final_mass_1)
                new_data.append(initial_mass_2)
                new_data.append(final_mass_2)

                new_data.append(initial_radius_1)
                new_data.append(final_radius_1)
                new_data.append(initial_radius_2)
                new_data.append(final_radius_2)

                new_data.append(initial_stellar_type_1)
                new_data.append(final_stellar_type_1)
                new_data.append(initial_stellar_type_2)
                new_data.append(final_stellar_type_2)

                new_data.append(initial_disk)
                new_data.append(final_disk)

                new_data.append(initial_stability)
                new_data.append(final_stability)
                
                new_data.append(initial_type)
                new_data.append(final_type)

                new_data.append(initial_accretor)
                new_data.append(final_accretor)
                new_data.append(initial_donor)
                new_data.append(final_donor)

                new_data.append(initial_separation)
                new_data.append(final_separation)

                initially_stable_MT_data.append(new_data)


                #### Extra error messages:
                second_disk = second_timestep[disk_index]

                if not int(initial_disk) == int(second_disk):
                    print("ERROR FOUND ONE WHERE FIRST DISK ISNT EQUAL TO SECOND_DISK: {filename}".format(filename=datafile))
                    wrong_disc_line_files += 1



    print('Found {} unstable (1-liner) systems. Found {} initially stable systems.'.format(unstable_MT_count, initially_stable_MT_count))
    print("Also found {} files where the first and second line dont match qua disk or not".format(wrong_disc_line_files))
    print('writing data to hdf5 files')


    h5filename = os.path.join(DATA_DIR, 'rlof_initial_final_data.hdf5')

    # Write data
    initially_stable_df = pd.DataFrame(data=initially_stable_MT_data, columns=new_header)


    # We need to convert the columns to the correct datatypes:
    types_convert_dict = {el:'float' for el in initially_stable_df.columns.values.tolist()}
    types_convert_dict['filename'] = 'str'

    initially_stable_df = initially_stable_df.astype(types_convert_dict)
    initially_stable_df.to_hdf(h5filename, key='stable_df', mode='w')

    # Write settings
    h5_file = h5py.File(h5filename, 'r+')

    settings_file = os.path.join(DATA_DIR, settings_files[0])

    with open(settings_file, "r") as settings_file:
        settings_json = json.load(settings_file)

    # Create settings group
    settings_grp = h5_file.create_group("settings")

    # Write version_string to settings_group
    settings_grp.create_dataset("used_settings", data=json.dumps(settings_json))

    print("processing this took {}s".format(time.time()-processing_start))
