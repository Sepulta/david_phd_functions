# File that stores a dict with personal. Will only put things in here that are non default. (with versio 2.1.6 now)
# Tip: ./binary_c help will show you the values and explanation for these parameter he!

personal_defaults = {
    'max_neutron_star_mass': 2.1,
    'RLOF_method': 3, #3=Claeys #1; # 1= adaptive Coen's suggestion to prevent forced conservative mass loss and reproduce Schneider et al. 2015
    'max_evolution_time': 13700,
    'nonconservative_angmom_gamma': "RLOF_NONCONSERVATIVE_GAMMA_ISOTROPIC", # -1 for J1, -2 for J2, TODO: add -3 other values
    'minimum_timestep': 1e-6,

    # 'evolution_splitting': 0 # 0: no splitting, 1: splitting evolution for multiple SN kicks
    # 'evolution_splitting_sn_n': 0, # kicks per system
    # 'evolution_splitting_maxdepth': 20, #10 # max number of kicks allowed
    # 'vrot1': 0.0 #-3.0; #MATHIEU_WARNING: -3 for Ramirez-Agudelo et al. 2015 nothing or zero for Hurley et al. 2000
    # 'vrot2': 0.0 #-3.0; #MATHIEU_WARNING: -3 for Ramirez-Agudelo et al. 2015 nothing or zero for Hurley et al. 2000
    # 'POST_SN_ORBIT': 1 # TODO: consider this again
    # 'alpha_ce': 1, # 1 default =1 

    # TODO: GO through the kicks
    'sn_kick_dispersion_II': 265, # km s-1 
    'sn_kick_distribution_II': "KICK_VELOCITY_MAXWELLIAN", # prescription. 1 = maxwellian

    # See for the following:     # ECAP, NS_NS, IBC, GRB_Collapsar, TZ, AIC_BH, BH_BH, BH_NS, IA_hybrd_HEcoWD, IA_Hybrid_HeCOWD_subluminous
    # TODO: look at ultra stripped.
    # TODO: look at the other companion values. 
    'wr_wind': "WR_WIND_NUGIS_LAMERS", # uses Nugis & Lamers for WRs
    'BH_prescription': "BH_FRYER12_DELAYED", #Fryer et al 2012 rapid. 

    # grid options
    # 'nice': 'nice -n +20', # nice command e.g. 'nice -n +10' or '' 
    # 'timeout': 15, # seconds until timeout
}

    # # TODO: figure out what to do with this.
    # # $binary_grid::bse_options{'lambda_min'}=0.0;
    # $binary_grid::bse_options{'companion_SN_deltaM'}=1; # 0 = no companion stripping/accretion when SN happens
    #                                                     # 1 = use fit from Liu et al. 2015 for 3.5Msun main sequence companion
    #                                                     # other recipes which could be coded: Pan et al. 2012
    # $binary_grid::bse_options{'do_fallback_kick_scaling'} = 1; #1=Yes, default, 0=NO
    # # 'lambda_ce' #TODO: check this. 
    # # 'lambda_ionisation': 0.0 # TODO: check this
    # $binary_grid::bse_options{'acc2'}=1.5;
    # $binary_grid::bse_options{'low_mass_NS_kicks'} = 1; #1=Yes, default, 0=NO
    # $binary_grid::bse_options{'do_BH_momentum_kicks'} = 0; #1=Yes, 0=NO  default
    # $binary_grid::bse_options{'delta_mcmin'}=0.0;
    # $binary_grid::bse_options{'minimum_envelope_mass_for_third_dredgeup'}=0.5;


    # # TODO: go through the ones below.
    # $binary_grid::bse_options{'beta_RLOF'} = 0.5; #-1; # means use thermal limit ; # use number between 0 and 1 for fixed
    # $binary_grid::bse_options{'rotationally_enhanced_mass_loss'}=1; # MATHIEU_WARNING: what is this? (prevent spun up accretion)

    # $binary_grid::bse_options{'lbv_prescription'}=1; #0 for hurley 2002, 1 for belcynski 2015 (!DEFINE DAVID_LBV)
    # $binary_grid::bse_options{'ppisn_prescription'}=0; #0 for no ppisn, 1 for belcynski 2016, 2 for spera et al. 2017 (!DEFINE DAVID_PPISN)
    
    # $binary_grid::bse_options{'qcrit_LMMS'}=1/0.56; 
    # $binary_grid::bse_options{'qcrit_MS'}=1/0.65;
    # $binary_grid::bse_options{'qcrit_HG'}=1/0.4;
    # $binary_grid::bse_options{'qcrit_GB'}=1/0.25;
    # $binary_grid::bse_options{'qcrit_CHeB'}=1/0.25;
    # $binary_grid::bse_options{'qcrit_EAGB'}=1/0.25;
    # $binary_grid::bse_options{'qcrit_TPAGB'}=1/0.25;
    # $binary_grid::bse_options{'qcrit_HeMS'}=1/0.25;
    # $binary_grid::bse_options{'qcrit_HeWD'}=1/0.25;
    # $binary_grid::bse_options{'qcrit_COWD'}=1/0.25;
    # $binary_grid::bse_options{'qcrit_ONeWD'}=1/0.25;
    # $binary_grid::bse_options{'qcrit_NS'}=1/0.25;
    # $binary_grid::bse_options{'qcrit_BH'}=1/0.25;