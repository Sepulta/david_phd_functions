import inspect

def get_caller_name():
    try:
        caller_name = inspect.currentframe().f_back.f_code.co_name
    except:
        caller_name = ""

    return caller_name