
import h5py

def print_attrs(name, obj):
    print (name)
    for key, val in obj.attrs.items():
        print("    %s: %s" % (key, val))

def print_content_hdf5(filename):
    f = h5py.File(filename, 'r')
    f.visititems(print_attrs)
