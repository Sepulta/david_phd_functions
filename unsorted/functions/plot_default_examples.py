import numpy as np
# Custom functions
from general_functions import get_data_single

#Include these for plotting
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator
from matplotlib import cm

# minorLocator = AutoMinorLocator(2)

# # dummy data
# #extracted_data_1solmet = get_data_single('../datafiles/single_fallback/single_fallback_analysis_0.02_2017-10-22_18:04:11.dat',  '\t\t\t\t\t')
# extracted_data_1solmet = get_data_single('../datafiles/bigrange_sim/single_bigrange500_0.0001_20171101_134303.dat',  '\t\t\t\t\t')

# df_ns = extracted_data_1solmet[1]
# df_bh = extracted_data_1solmet[2]
# print df_bh.columns



#solar metallicity
#label=r'1.0 $Z_{\odot}$


# # ========================= Histogram single result plot
# plt.hist(masses_bh_1solmet, weights=weights_bh_1solmet, normed=1, bins=50)
# plt.title("Single star blackhole distribution (%d total points) at solar metallicity"%len(masses_bh_1solmet))
# plt.ylabel("Fraction of blackholes per bin")
# plt.xlabel(r'Blackhole remnant mass [$M_{\odot}$]')
# # plt.savefig('blackhole_distribution_1solmet_2500.jpg')
# plt.show()

# # ========================= Histogram combined result plot
# plt.hist(masses_bh_001solmet, weights=weights_bh_001solmet, 
# 	normed=1, 
# 	bins=50, 
# 	linestyle='--', 
# 	histtype='step', 
# 	label='0.01 Solar metallicity',
# 	linewidth=2.0
# 	)
# plt.hist(masses_bh_01solmet, weights=weights_bh_01solmet, 
# 	normed=1, 
# 	bins=50, 
# 	linestyle='-.', 
# 	histtype='step', 
# 	label='0.1 Solar metallicity',
# 	linewidth=2.0
# 	)
# plt.hist(masses_bh_1solmet, weights=weights_bh_1solmet, 
# 	normed=1, 
# 	bins=50, 
# 	histtype='step', 
# 	label='Solar metallicity',
# 	linewidth=2.0
# 	)
# legend = plt.legend()
# legend.get_frame().set_facecolor('none')
# legend.get_frame().set_linewidth(0.0)
# plt.title("Single star blackhole distribution at multiple metallicities", fontsize=20)
# plt.ylabel("Fraction of blackholes per bin", fontsize=20)
# plt.xlabel(r'Blackhole remnant mass [$M_{\odot}$]', fontsize=20)
# plt.axes().xaxis.set_minor_locator(AutoMinorLocator(2))
# plt.axes().yaxis.set_minor_locator(AutoMinorLocator(2))
# plt.axes().xaxis.set_tick_params(which='minor', length=4)
# plt.axes().yaxis.set_tick_params(which='minor', length=4)
# # plt.savefig('blackhole_distribution_combined_2500.jpg', bbox_inches='tight')
# plt.show()

# # ========================= Normal Plot single result
# plt.plot(initial_mass_bh_1_solmet, masses_bh_1solmet,'ro')
# plt.title(r'Initial vs. Final mass blackhole (%d points) at 1.0 [$Z_{\odot}$]'%len(initial_mass_bh_1_solmet))
# plt.ylabel(r'Blackhole remnant mass [$M_{\odot}$]')
# plt.xlabel(r'Initial stellar mass [$M_{\odot}$]')
# # plt.savefig('initial_vs_final_mass_1solmet.jpg')
# plt.show()


# # ========================= Normal plot combined result 
# plt.plot(initial_mass_bh_1_solmet, masses_bh_1solmet,'r', label=r'1.0 $Z_{\odot}$', markeredgewidth=0.0)
# plt.plot(initial_mass_bh_01_solmet, masses_bh_01solmet,'g', label=r'0.1 $Z_{\odot}$', markeredgewidth=0.0)
# plt.plot(initial_mass_bh_001_solmet, masses_bh_001solmet,'b', label=r'0.01 $Z_{\odot}$', markeredgewidth=0.0)
# legend = plt.legend(loc=2)
# legend.get_frame().set_facecolor('none')
# legend.get_frame().set_linewidth(0.0)
# plt.title(r'Initial vs. Final mass blackhole at multiple metallicities', fontsize=20)
# plt.ylabel(r'Blackhole remnant mass [$M_{\odot}$]', fontsize=20)
# plt.xlabel(r'Initial stellar mass [$M_{\odot}$]', fontsize=20)
# plt.axes().xaxis.set_minor_locator(AutoMinorLocator(2))
# plt.axes().yaxis.set_minor_locator(AutoMinorLocator(2))
# plt.axes().xaxis.set_tick_params(which='minor', length=4)
# plt.axes().yaxis.set_tick_params(which='minor', length=4)
# # plt.savefig('initial_vs_final_mass_combined_line_2500.jpg', bbox_inches='tight')
# plt.show()


# # ========================= Normal plot with colorbar
# dataframe = df_bh
# #cmap = cm.get_cmap('jet', len(set(dataframe["prev stellar_type"])))
# cmap = cm.get_cmap('jet', 15)
# #sc = plt.scatter(dataframe["pms_mass"], dataframe["m1"], 
# #	c=dataframe["prev stellar_type"], lw = 0, cmap=cmap, \
# #	vmin=min(dataframe["prev stellar_type"]), vmax=max(dataframe["prev stellar_type"]))
# sc = plt.scatter(dataframe["pms_mass"], dataframe["m1"], 
# 	c=dataframe["prev stellar_type"], lw = 0, cmap=cmap, \
# 	vmin=1, vmax=15)
# #plt.colorbar(sc, ticks=range(min(dataframe["prev stellar_type"]), max(dataframe["prev stellar_type"])+1))
# plt.colorbar(sc, ticks=range(1, 16))
# plt.show()


# #Mathieu's example
# def make2Dmap(x,y,Prob, x1, x2, y1, y2):
# 	minx = min(min(x),x1)
# 	maxx = min(max(x),x2)
# 	miny = min(min(y),y1)
# 	maxy = min(max(y),y2)

# 	x_int = np.linspace(minx,maxx,40)
# 	y_int = np.linspace(miny,maxy,40)

# 	mat = np.zeros([len(x_int),len(y_int)])
# 	for i in xrange(0,len(x_int)-1):
# 	    for j in xrange(0,len(y_int)-1):
# 	        mat[j,i] = np.sum(Prob[(x>=x_int[i])*(x<x_int[i+1])*(y>=y_int[j])*(y<y_int[j+1])])
# 	return x_int, y_int, mat

# x1,x2 = ax22.get_xlim()
# y1,y2 = ax11.get_xlim()
# X,Y,mat = make2Dmap(x,y,Prob, x1,x2,y1,y2)
# ax12.pcolor(X,Y,mat,norm=None,cmap=CMAP)
# ax12.set_xlim(min(X),max(X))
# ax12.set_ylim(ax11.get_xlim())


# fig = plt.figure(figsize=(25., 25.))
# gs = gridspec.GridSpec(500,500)
# ax11 = plt.subplot(gs[0:100,0:100])
# # ax11.text(0.5,0.5,"ax11",transform=ax11.transAxes)
# ax12 = plt.subplot(gs[0:100,100:200])
# # ax12.text(0.5,0.5,"ax12",transform=ax12.transAxes)
# ax13 = plt.subplot(gs[0:100,200:300])
# # ax13.text(0.5,0.5,"ax13",transform=ax13.transAxes)
# ax14 = plt.subplot(gs[0:100,300:400])
# # ax14.text(0.5,0.5,"ax14",transform=ax14.transAxes)
# ax15 = plt.subplot(gs[0:100,400:500])