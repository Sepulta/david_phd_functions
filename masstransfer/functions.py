"""
Module containing functions for mass transfer

TODO: add the correct functions here. 
"""

import numpy as np

def rochelobe():

    pass

def lagrange_points():
    pass

def rochelobe_radius_accretor(mass_donor, mass_accretor):
    """
    Function to calculate the roche lobe radius of the accretor

    value is normalised by separation

    from FKR 4.6 modified.
    """

    top = 0.49 * (mass_accretor/mass_donor) ** (2.0/3.0)
    bottom = (0.6 * (mass_accretor/mass_donor) ** (2.0/3.0)) + np.log(1 + (mass_accretor/mass_donor) ** (1.0/3.0))
    R2 = top/bottom

    return R2

def rochelobe_radius_donor(mass_donor, mass_accretor):
    """
    Function to calculate the roche lobe radius of the donor

    value is normalised by separation

    from FKR 4.6
    """

    top = 0.49 * (mass_donor/mass_accretor) ** (2.0/3.0)
    bottom = (0.6 * (mass_donor/mass_accretor) ** (2.0/3.0)) + np.log(1 + (mass_donor/mass_accretor) ** (1.0/3.0))
    R2 = top/bottom

    return R2

def b1_fkr(mass_donor, mass_accretor):
    """
    Function to calculate the distance from accretor center to l1 point where mass is flowing through

    value is normalised by separation

    from FKR 4.9. The value should be log10. not log
    """

    return 0.500 -0.227*np.log10(mass_donor/mass_accretor)


def rcirc_fkr(mass_donor, mass_accretor):
    """
    Function to calculate the circularisation radius, normalised by separation.

    from FKR 20
    """

    b1_val = b1_fkr(mass_donor, mass_accretor)

    r_circ = (1 + (mass_donor/mass_accretor)) * (b1_val**4)

    return r_circ


def rmin_fkr(mass_donor, mass_accretor):
    """
    Function to calculate the circularisation radius, normalised by separation.

    from FKR 20
    """

    rcirc = rcirc_fkr(mass_donor, mass_accretor)
    rmin = rcirc/1.7

    return rmin

def rmin_ulrich_burger(mass_donor, mass_accretor):
    """
    Radius of closest approach of a Roche stream
    to an accreting star.

    Eq.(1) of Ulrich & Burger (1976, ApJ, 206, 509)

    fitted to the calculations of Lubow & Shu (1975, ApJ, 198, 383).

    q is Mdetached/Mcontact = Maccretor/Mdonor
    """

    q = mass_accretor/mass_donor

    return 0.04250 * ((q * (1.0 + q))**(1/4))


def rcirc_ulrich_burger(mass_donor, mass_accretor):
    """
    Radius of circularisation of mass stream
    """

    rmin = rmin_ulrich_burger(mass_donor, mass_accretor)

    rcirc = 1.7 * rmin

    return rcirc

def return_case_masstransfer(stellar_type):
    """
    Function to return the mass transfer case
    """
    MAIN_SEQUENCE = 1
    HeWD = 10
    CHeB = 4
    
    if stellar_type <= MAIN_SEQUENCE:
        # main sequence, RLOF_A
        rlof_type = 'a'
    elif (stellar_type < HeWD):
        # H-Shell burning or HE burning:
        rlof_type = 'c' if stellar_type >= CHeB else 'b'
    else:
        # Probably rlof from compact object
        rlof_type = 'd'
    return stellar_type, rlof_type