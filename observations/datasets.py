"""
Module containing class and subclasses to load in datasets
"""
import os
import numpy as np
import pandas as pd
from astropy.table import Table

class DataLoader(object):
    """
    Class to load data
    """

    def __init__(self, name):
        self.name = name


    def load_data(self, ):
        """
        Function that loads the dataset and puts it into a dataframe
        """
        raise NotImplementedError

class Harries2003Loader(DataLoader):
    def __init__(self):
        self.name='harries2003'
        self.data_dir=os.path.join(os.environ['PHD_DIR'], 'projects/eclipsing_binaries_data/datafiles/harries_2003')

    def load_data(self):
        dataframe = pd.ExcelFile(os.path.join(self.data_dir, 'harries_2003.xlsx'))
        return dataframe

class Hilditch2004Loader(DataLoader):
    def __init__(self):
        self.name='hilditch2004'
        self.data_dir=os.path.join(os.environ['PHD_DIR'], 'projects/eclipsing_binaries_data/datafiles/hilditch_2004/')

    def load_data(self):
        dataframe = pd.ExcelFile(os.path.join(self.data_dir, 'hilditch_2004.xlsx'))
        return dataframe

class Budding2004Loader(DataLoader):
    def __init__(self):
        self.name='budding2004'
        self.data_dir=os.path.join(os.environ['PHD_DIR'], 'projects/eclipsing_binaries_data/datafiles/budding_2004/')

    def load_data(self):
        # Table 1
        dat_table_1 = Table.read(os.path.join(self.data_dir, 'fits/J_A+A_417_263_table1.dat.fits'), format='fits')    
        df_table_1 = dat_table_1.to_pandas()
        # print(df_table_1.head())
        
        # table 2
        dat_table_2 = Table.read(os.path.join(self.data_dir, 'fits/J_A+A_417_263_table2.dat.fits'), format='fits')    
        df_table_2 = dat_table_2.to_pandas()
        # print(df_table_2.head())

        # table 3
        dat_table_3 = Table.read(os.path.join(self.data_dir, 'fits/J_A+A_417_263_table3.dat.fits'), format='fits')    
        df_table_3 = dat_table_3.to_pandas()
        # print(df_table_3.head())

        # table 4
        dat_table_4 = Table.read(os.path.join(self.data_dir, 'fits/J_A+A_417_263_table4.dat.fits'), format='fits')    
        df_table_4 = dat_table_4.to_pandas()
        # print(df_table_4.head())  

        # Table 5
        dat_table_5 = Table.read(os.path.join(self.data_dir, 'fits/J_A+A_417_263_table5.dat.fits'), format='fits')    
        df_table_5 = dat_table_5.to_pandas()
        # print(df_table_5.head())

        # Table 6a
        dat_table_6a = Table.read(os.path.join(self.data_dir, 'fits/J_A+A_417_263_table6a.dat.fits'), format='fits')    
        df_table_6a = dat_table_6a.to_pandas()
        # print(df_table_6a.head())
        
        # Table 6b
        dat_table_6b = Table.read(os.path.join(self.data_dir, 'fits/J_A+A_417_263_table6b.dat.fits'), format='fits')    
        df_table_6b = dat_table_6b.to_pandas()
        # print(df_table_6b.head())

        # Table 6c
        dat_table_6c = Table.read(os.path.join(self.data_dir, 'fits/J_A+A_417_263_table6c.dat.fits'), format='fits')    
        df_table_6c = dat_table_6c.to_pandas()
        # print(df_table_6c.head())
        
        # Table 6c
        dat_table_6d = Table.read(os.path.join(self.data_dir, 'fits/J_A+A_417_263_table6d.dat.fits'), format='fits')    
        df_table_6d = dat_table_6d.to_pandas()

        return {'table_1': df_table_1, 'table_2': df_table_2, 'table_3': df_table_3, 'table_4': df_table_4, 'table_5': df_table_5, 'table_6a': df_table_6a, 'table_6b': df_table_6b, 'table_6c': df_table_6c, 'table_6d': df_table_6d}




harries=Harries2003Loader()
harries_data = harries.load_data()

hilditch=Hilditch2004Loader()
hilditch_data = hilditch.load_data()
print(hilditch_data.sheet_names)

print(dir(hilditch_data))

budding = Budding2004Loader()
budding_data = budding.load_data()
# print(budding_data)



quit()





