# -----------------------------------------------------------#
# author: Mathieu Renzo
# launch a pyhton notebook on <remote host>, and sets up
# port forwarding and tunneling.
# First argument should be the port to forward from <remote host>.
# Second argument should be the port to listen to on
# <localhost>
# Assumes you have encrypted keys log in enabled (no passwd).
# synopsis: sh ./taurus_ipython $RANDOM $RANDOM
# (or two integers instead of $RANDOM $RANDOM)
#------------------------------------------------------------#

# open iPython kernel on a given port
echo "you should point me to your virtual environment and NOTEBOOK DIRECTORY!! "
#ssh -f mathieu@taurus.science.uva.nl 'source /scratch/mathieu/venv/bin/activate && /scratch/mathieu/anaconda2/bin/jupyter-notebook --no-browser --port='$1' --notebook-dir=/scratch/mathieu/MMPS/SCRIPTS_dirty/'
ssh -f davidh@taurus.science.uva.nl 'source /home/davidh/venv_thesis/bin/activate && /home/davidh/venv_thesis/bin/jupyter-notebook --no-browser --port='$1' --notebook-dir=/scratch/davidh/masterproject_david_hendriks/'

# tunnel the connection
#ssh -f -N -L $2:localhost:$1 mathieu@taurus.science.uva.nl
ssh -f -N -L $2:localhost:$1 davidh@taurus.science.uva.nl

#print the port (for future reference)
echo "=============="
echo $2
echo "=============="
# ----------------------------------------------------------------------
# open the iPython kernel in the local browser
firefox "127.0.0.1:"$2  ## to use firefox browser to edit the notebook
# ----------------------------------------------------------------------
# open the iPython kernel in emacs
# ein:notebooklist-open requires to install ein from the MELPA repositories in emacs
# -mm maximises the emacs window
# emacs -mm --eval "(ein:notebooklist-open http://127.0.0.1:"$2")" 
# emacs -mm -eval '(ein:notebooklist-open localhost:'$2')'
# ----------------------------------------------------------------------
