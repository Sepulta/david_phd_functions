"""
Function to find the edge

Idea is to have a function vary input values (keep it 1d) and find the exact location of the edge.
we want the left and right values to be on the left and right site of the edge, and we want to get these left and right values as close as possible

The initial guesses should boil down to a 'True' (left) and 'False' (right).

TODO: add option to pass extra args to the function
"""

import numpy as np

def dummy_edge(input_value):
    """
    function to act as a dummy output
    """


    if input_value < 20:
        return True

    else:
        return False


def edge_finder(initial_input_list, output_function, diff_threshold, function_args=None):
    """
    Function that can zoom in onto an edge
    """

    max_tries = 100
    tries = 0
    finding = True
    matched_threshold = False

    #
    left_guess = initial_input_list[0]
    right_guess = initial_input_list[1]

    #
    initial_left_output = output_function(left_guess)
    initial_right_output = output_function(right_guess)

    # Check values of initial guesses
    if initial_left_output == initial_right_output:
        msg = "Both the left and the right guess have the same result. Abort"
        raise ValueError(msg)

    # Check for nones
    for tryout in range(10):
        if initial_left_output is None:
            print("Found None value for left guess. Moving it to the right")
            left_guess += 1
            initial_left_output = output_function(left_guess)
        else:
            break
    if initial_left_output is None:
        print("Left value still None. Abort")
        raise ValueError

    for tryout in range(10):
        if initial_right_output is None:
            print("Found None value for right guess. Moving it to the left")
            right_guess -= 1
            initial_right_output = output_function(right_guess)
        else:
            break
    if initial_right_output is None:
        print("Right value still None. Abort")
        raise ValueError

    # 
    left_output = initial_left_output
    right_output = initial_right_output
    # print("Currently edge located between: {} and {}".format(left_guess, right_guess))

    while finding:
        # calculate distance and propose new positions
        distance = np.abs(left_guess-right_guess)

        #
        new_left_guess = left_guess + distance/2
        new_right_guess = right_guess - distance/2

        #
        left_output = output_function(new_left_guess)

        #
        if [left_output, right_output] == [initial_left_output, initial_right_output]:
            # print("Going from right = {} to right = {}".format(left_guess, new_left_guess))
            left_guess = new_left_guess
        else:
            right_output = output_function(new_right_guess)

            # print("Going from right = {} to right = {}".format(right_guess, new_right_guess))
            right_guess = new_right_guess
        # print("Currently edge located between: {} and {}".format(left_guess, right_guess))
        tries += 1

        ########
        # Control of while loop
        if tries > max_tries:
            finding = False

        if np.abs(left_guess-right_guess)<diff_threshold:
            finding = False
            matched_threshold = True

    #
    print("\tFinal locations of edge: {} and {}".format(left_guess, right_guess))

    status_dict = {
        'tries': tries,
        'matched_threshold': matched_threshold,
        'left_guess': left_guess,
        'right_guess': right_guess,
    }

    return status_dict

# status = edge_finder([15, 30], dummy_edge, 1e-5)
# print(status)
