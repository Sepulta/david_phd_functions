def make2Dmap(x,y,Prob, x1, x2, y1, y2, bins):
    """
    Function to make a 2d map for density plots
    """

    minx = min(min(x), x1)
    maxx = min(max(x), x2)
    miny = min(min(y), y1)
    maxy = min(max(y), y2)

    x_int = np.linspace(minx, maxx, bins)
    y_int = np.linspace(miny, maxy, bins)

    mat = np.zeros([len(x_int), len(y_int)])
    for i in range(0, len(x_int) - 1):
       for j in range(0, len(y_int) - 1):
           mat[j,i] = np.sum(Prob[(x >= x_int[i]) * (x < x_int[i+1]) * (y >= y_int[j]) * (y < y_int[j+1])])
    return x_int, y_int, mat
    
def make2Dmap_new(x,y,Prob, x1, x2, y1, y2, bins):
    """
    Function to make a 2d map for density plots where the boundary values are explicitly used
    """
    x_int = np.linspace(x1, x2, bins)
    y_int = np.linspace(y1, y2, bins)

    mat = np.zeros([len(x_int), len(y_int)])
    for i in range(0, len(x_int) - 1):
        for j in range(0, len(y_int) - 1):
            mat[j,i] = np.sum(Prob[(x >= x_int[i]) * (x < x_int[i+1]) * (y >= y_int[j]) * (y < y_int[j+1])])
    return x_int, y_int, mat

def get_2Dmap(df, parameter_1, parameter_2, bins):
    """
    Function to make a density plot using a 2d map function
    """

    X, Y, mat = make2Dmap(
            x=df[parameter_1], 
            y=df[parameter_2], 
            Prob=df['probability'],
            x1=min(df[parameter_1]),
            x2=max(df[parameter_1]),
            y1=min(df[parameter_2]),
            y2=max(df[parameter_2]),
            bins=bins)
    return X, Y, mat

def make2Dmap_new(x,y,Prob, minx, maxx, miny, maxy, bins):
    x_int = np.linspace(minx, maxx, bins)
    y_int = np.linspace(miny, maxy, bins)

    mat = np.zeros([len(x_int), len(y_int)])
    for i in range(0, len(x_int) - 1):
        for j in range(0, len(y_int) - 1):
            mat[j,i] = np.sum(Prob[(x >= x_int[i]) * (x < x_int[i+1]) * (y >= y_int[j]) * (y < y_int[j+1])])
    return x_int, y_int, mat


def lower_diag_matrix(mat):
    shape = mat.shape
    x_dim = shape[0]
    y_dim = shape[1]
    for i in range(y_dim):
        for j in range(x_dim):
            val_i_j = mat[i,j]
            if not val_i_j == 0:
                if i > j:
                    mat[j,i] += mat[i,j]
                    mat[i,j] = 0
    return mat