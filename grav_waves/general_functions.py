import math
import numpy as  np

def calc_total_mass(m1, m2):
    """
    Function to calculate total mass
    """
    return m1+m2

def calc_chirpmass(m1, m2):
    """
    Function to calculate chirpmasses. Check the input beforehand.
    """
    chirpmass = pow((m1*m2), 3.0/5.0)/pow((m1+m2), 1.0/5.0)
    return chirpmass

def calc_reduced_mass(m1, m2):
    """
    Function to calculate reduced mass of a system
    """
    return (m1*m2)/(m1+m2)

def calc_inspiral_time(m1, m2, orbital_period, e):
    """
    Calculate the inspiral time for the merger
    Initially this is done by using the assumption that the orbit is circular
    input:
    - m1:                               Mass of star 1, in solarmass
    - m2:                               Mass of star 2, in solarmass
    - orbital_period:                   In years
    - e:                                Eccentricity of the system


    returns the inspiral time in years
    Based on formula from Peters
    """

    #Convert the orbital_period to hours
    orbital_period_in_hours = orbital_period*365.25*24
    
    t_inspiral = pow(10, 7) * \
    pow(orbital_period_in_hours, 8.0/3.0) * \
    pow(calc_total_mass(m1, m2), -2.0/3.0) * \
    pow(calc_reduced_mass(m1, m2), -1.0) * \
    pow(1-pow(e, 2.0), 7.0/2.0)

    return t_inspiral

################
# Data processing
def preprocessor_startrack(filename):
    """
    Function to pre-process synthetic universe files
    """
    dataset = open(filename, 'r')
    processed_data = []
    for line in dataset:
        splitted = line.split(' ')
        data = splitted[:20]
        evol_hist = " ".join(splitted[20:][:-1])
        dataline = data
        dataline.append(evol_hist)
        processed_data.append(dataline)
    return processed_data