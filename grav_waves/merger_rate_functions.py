import numpy as np

# Idea: lets see how many there are created per solarmass.

# Steps: For a given dataset. We have the total probabilty of occurence of the systems that we are interested in. 

# First we calculate the amt per solarmass:


def sampling_from_IMF(Nsamples, max_mass):
    # From FLoor Broekgaarden initially
    kroupaPower1, kroupaPower2, kroupaPower3 = -0.3, -1.3, -2.3

    # // Often require the np.powerer law exponent plus one
    kroupaPowerPlus1_1, kroupaPowerPlus1_2, kroupaPowerPlus1_3 = 0.7, -0.3, -1.3

    # // There are two breaks in the Kroupa np.power law -- they occur here (in solar masses)
    kroupaBreak1, kroupaBreak2 = 0.08, 0.5

    # else if(initialMassFunctionMin > kroupaBreak1 and initialMassFunctionMin <= kroupaBreak2 and initialMassFunctionMax > kroupaBreak2){
    initialMassFunctionMin, initialMassFunctionMax = 0.08, max_mass

    firstBrackets = (1.0/kroupaPowerPlus1_2)*(np.power(kroupaBreak2, kroupaPowerPlus1_2) - np.power(initialMassFunctionMin, kroupaPowerPlus1_2))
    secondBrackets = (1.0/kroupaPowerPlus1_3)*(np.power(initialMassFunctionMax, kroupaPowerPlus1_3) - np.power(kroupaBreak2, kroupaPowerPlus1_3))

    C = firstBrackets + secondBrackets #  C is the normalisation constant for the integral of the IMF over the desired range

    # Sample N random 0's and 1's
    u =  np.random.uniform(0, 1, Nsamples) # will hold a uniform random number U(0,1) #// Draw a random number between 0 and 1

    # Create array which will hold the sampled masses
    thisMass = np.zeros_like(u)

    # This sort off mimics a else if statement, but then with masks and 
    #  Create two masks
    mask1 = (u < firstBrackets/C)
    mask2 = (u >= firstBrackets/C)

    # Apply the calculation of the 
    thisMass[mask1] = np.power(((C * u[mask1] * kroupaPowerPlus1_2) + np.power(initialMassFunctionMin, kroupaPowerPlus1_2)), (1.0/kroupaPowerPlus1_2))
    thisMass[mask2] = np.power(((((C * u[mask2]) - firstBrackets)*kroupaPowerPlus1_3) + np.power(kroupaBreak2, kroupaPowerPlus1_3)), (1.0/kroupaPowerPlus1_3))

    return thisMass

def sampling_from_q(Nsamples):
    a_q = 0.1
    b_q = 1.0

    '''returns  N samples distributed uniformly '''
    return np.random.uniform(a_q, b_q, Nsamples)

def calculate_probabilty_per_solarmass(df, amt_stars=100, max_mass=150, fbin=0.7, print_info=True):
    """
    Function to calculate the probabilty per solarmass for a given event. 
    Takes into account binary fraction etc

    TODO: Properly account for the fact that we sampled in a smaller box than the MC sampling will do
    TODO: Look at the Demo_CalculateAbsoluteMergerRate blabla.
    """

    # Get sum of probabilities of all merging bhbh systems
    sum_prob = sum(df['probability'])

    # Get total mass of a distribution of 10**8 stars
    # calculate the entire mass in the Monte Carlo simulation.
    M1ZAMSrate = sampling_from_IMF(amt_stars, max_mass) # use a large number of draws from each distribution to get better precision

    # draw uniform from q
    qs = sampling_from_q(amt_stars)

    # calculate M2 masses
    M2ZAMSrate =  qs * M1ZAMSrate

    # now mask all binaries that fall in the sampled mass box. You should know these values, or read them out with the settings json
    maskSimulationm1 = ((M1ZAMSrate >= 1) & (M1ZAMSrate <= 150))

    # calculate the entire mass in the Monte Carlo simulation.
    TotalMassBinaries =  np.sum(M1ZAMSrate) + np.sum(M2ZAMSrate)
    AverageMassBinarySystem = TotalMassBinaries/amt_stars

    # # Calculate the mass from the MC sampling that belongs to the simulation box
    TotalMassBinariesSimulation = np.sum(M1ZAMSrate[maskSimulationm1]) + np.sum(M2ZAMSrate[maskSimulationm1])

    ## The fraction of mass of COMPAS to all mass from the entire parameter space is:
    fractionTotalMassBinariesSimulation = TotalMassBinariesSimulation / TotalMassBinaries

    # Binary fraction
    AmtSingles = amt_stars * ((1-fbin)/fbin)

    # Generate single stars based on binary fraction and total mass of binaries
    TotalMassSingles = np.sum(M1ZAMSrate)*((1-fbin)/fbin) # Get amount and mass of single stars in this whole thing. 

    # Calculate total mass
    TotalMassAll = TotalMassBinaries + TotalMassSingles
    
    # Calculate average mass of a star in a binary
    TotalAmtStars = 2 * amt_stars + AmtSingles

    # 
    AverageMassStar = TotalMassAll/(TotalAmtStars)

    # Calculate the probability of a merger per solarmass (amt all binaries, but divide by summed mass of binaries and singles)
    prob_per_solarmass = (sum_prob * amt_stars * fractionTotalMassBinariesSimulation)/TotalMassAll

    if print_info:
        print('the fraction of total mass evolved in this simulation: {}'.format(fractionTotalMassBinariesSimulation))

        print("Calculated probability per solar mass:")
        print("Total amt stars: %d"%TotalAmtStars)
        print("Total mass for %d stars: %f"%(amt_stars, TotalMassAll))

        print("Average mass of a star: %f"%(AverageMassStar))
        print("Total amt binaries: %d"%amt_stars)
        print("Average mass of a binary system: %f"%AverageMassBinarySystem)

        print("Summed probability of merging bhbhs: %f"%sum_prob)
        print("Probability per solar mass: {}".format(prob_per_solarmass))

    info_dict = {
        'AverageMassStar': AverageMassStar,
        'AverageMassBinarySystem': AverageMassBinarySystem,
        'sum_prob': sum_prob,
        'prob_per_solarmass': prob_per_solarmass
    }

    return info_dict

# def calculate_mergerrate_per_myr_and_rvol(df, title, amt_stars=100000000, max_mass=300):
#     """
#     Function to calculate the amount of occurences per MYR and per Rvol
#     """

#     merging_prob_per_solarmass = calculate_probabilty_per_solarmass(df, amt_stars=amt_stars, max_mass=max_mass, fbin=0.7)

#     # Star formation rate
#     SFR = 3.5 # solarmass per year

#     # 
#     mass_MWEG= SFR * pow(10, 10) # SFR * 10 gigayear

#     # Calculate amunt of mergers per milkyway equivalent galaxy per myr (or rather, multiply starformation rate by myr )
#     mergers_per_MWEG_per_myr = merging_prob_per_solarmass*SFR*pow(10, 6)

#     MWLG_density = 0.0116 # milkywaylike galaxy density (Petrillo et al. (2013), (Kim et al. 2006),)
    
#     #Calculate LIGO volumetric rate. 
#     R_vol = 10 * (0.0116/0.01) * merging_prob_per_solarmass*SFR*pow(10, 6) # see belczynski & de mink 2015
    
#     print("Probability of merging per solarmass: %0.12f"%merging_prob_per_solarmass)
#     print("Amount of mergers per MWEG per Myr: %f"%mergers_per_MWEG_per_myr)
#     print('Volumetric rate per year per gigaparsec cubed: %f'%R_vol)
#     return {'prob_per_solarmass': merging_prob_per_solarmass,
#             'merger_rate_MWEG_myr': mergers_per_MWEG_per_myr,
#             'rvol': R_vol
#            }


def calculate_mergerrate_per_myr_and_rvol(merging_prob_per_solar_mass, title, amt_stars=100000000, max_mass=300):
    """
    Function to calculate the amount of occurences per MYR and per Rvol
    """

    # Star formation rate
    SFR = 3.5 # solarmass per year

    #
    mass_MWEG= SFR * pow(10, 10) # SFR * 10 gigayear

    # Calculate amunt of mergers per milkyway equivalent galaxy per myr (or rather, multiply starformation rate by myr )
    mergers_per_MWEG_per_myr = merging_prob_per_solar_mass*SFR*pow(10, 6)

    MWLG_density = 0.0116 # milkywaylike galaxy density (Petrillo et al. (2013), (Kim et al. 2006),)

    #Calculate LIGO volumetric rate.
    R_vol = 10 * (0.0116/0.01) * merging_prob_per_solar_mass*SFR*pow(10, 6) # see belczynski & de mink 2015

    print("Probability of merging per solarmass: %0.12f"%merging_prob_per_solar_mass)
    print("Amount of mergers per MWEG per Myr: %f"%mergers_per_MWEG_per_myr)
    print('Volumetric rate per year per gigaparsec cubed: %f'%R_vol)
    
    return {
        'prob_per_solarmass': merging_prob_per_solar_mass,
        'merger_rate_MWEG_myr': mergers_per_MWEG_per_myr,
        'rvol': R_vol
    }
