"""
File containing functions to handle dataframe for the grav waves stuff
"""

import numpy as np
from david_phd_functions.grav_waves.general_functions import *

###################################
# General dataframe actions
###################################

def cut_df_real(df):
    """
    Function to cut off the interesting part of a binary evolution.

    Checks which star has undergone his last stellar type change last, 
    and splits the df to give back everything before and including that time

    BEWARE: this might not work if the 
    """

    # Check which remnant types they have
    remnant_type_1 = max(df['stellar_type_1'].values.tolist())
    remnant_type_2 = max(df['stellar_type_2'].values.tolist())

    # # Check which of the stars has been in its remnant type for the shortest, 
    # # as thats the one we need to use as the max
    len_remnant_phase_1 = df.query('stellar_type_1==%d'%remnant_type_1).shape[0]
    len_remnant_phase_2 = df.query('stellar_type_2==%d'%remnant_type_2).shape[0]

    # If star 2 has been in its remnant phase shorter than star 1, then cut off from when star 2 changed to its remnant type 
    if len_remnant_phase_2 < len_remnant_phase_1:
        time_last_change = df[df.stellar_type_2==remnant_type_2]['time'].values[0]
    # vice versa
    else:
        time_last_change = df[df.stellar_type_1==remnant_type_1]['time'].values[0]
    df_real = df[df.time<=time_last_change]

    return df_real

def val(dataframe_column):
    """
    Function to return the values of a dataframe column
    Output: list of values
    """

    return dataframe_column.values.tolist()

def split_dataframe(df, query_string):
    """
    Function to split the dataframe into 2 seperate dataframes. 
    The first which includes all the entries meeting the query
    The second is the remainder.  
    """

    #Copy dataframes
    including_df = df.copy()
    remainder_df = df.copy()

    #Query the including dataframe
    including_df = including_df.query(query_string)

    #Generate the remainder dataframe, having excluded the rows which are contained in the including_df
    remainder_df = remainder_df.drop(including_df.index.values)

    return including_df, remainder_df

def get_data_df(df, including_indices):
    """
    Function to return a dataframe based upon a set of indices 
    """
    all_indices = df.index.values
    excluding_indices = df.drop(list(including_indices)).index.values
    return df.drop(excluding_indices)

def split_dataframe_indices(df, query_string, including_subset_indices=[]):
    """
    Function to split the dataframe into 2 seperate dataframes. 
    The first which includes all the entries meeting the query
    The second is the remainder.
    
    The idea is that we use the main dataframe, and use queries to split off the indices
    Optionally we can give a subset of indices to exclude (OR MAYBE INCLUDE>>> WTF)
    """
    if len(including_subset_indices) > 0:
        excluding_indices = df.drop(including_subset_indices).index.values
        including_df_indices = df.drop(excluding_indices).query(query_string).index.values
        remainder_df_indices = df.drop(excluding_indices).drop(including_df_indices).index.values
    else:
        including_df_indices = df.query(query_string).index.values
        remainder_df_indices = df.drop(including_df_indices).index.values
    return including_df_indices, remainder_df_indices

###
def get_data_binary(filename, separator):
    """
    Function to readout binary star systems
    """

    #Readout the file
    df = pd.read_csv(filename, sep=separator, header=0, engine='python')
    
    #Remove duplicates
    df = df.drop_duplicates()
    
    #Filter on stellar type combination
    df_nsns = df.query('Stellar_type_1==13 and Stellar_type_2==13')
    df_nsbh = df.query('Stellar_type_1==13 and Stellar_type_2==14')
    df_bhns = df.query('Stellar_type_1==14 and Stellar_type_2==13')
    df_bhbh = df.query('Stellar_type_1==14 and Stellar_type_2==14')
    
    return df.columns.values.tolist(), df_nsns, df_nsbh, df_bhns, df_bhbh, df

def get_data_simple(filename, separator):
    """
    Function to readout binary star systems
    """

    #Readout the file
    df = pd.read_csv(filename, sep=separator, header=0, engine='python')
    
    #Remove duplicates
    df = df.drop_duplicates()
        
    return df.columns.values.tolist(), df

def get_data_single(filename, separator):
    """
    Function to readout single star systems
    """

    #Readout the file
    df = pd.read_csv(filename, sep=separator, header=0, engine='python')
    
    #Remove duplicates
    df = df.drop_duplicates()

    #Filter on stellar type endproduct
    df_ns = df.query('stellar_type==13')
    df_bh = df.query('stellar_type==14')
    
    return df.columns.values.tolist(), df_ns, df_bh

def get_data(filename, separator):
    """
    Function to readout any star system, and simply output the df
    """

    #Readout the file
    df = pd.read_csv(filename, sep=separator, header=0, engine='python')
    
    #Remove duplicates
    df = df.drop_duplicates()
        
    return df.columns.values.tolist(), df

def get_data_detections(filename):
    """
    Function to redout file containing gravitational wave detections
    """

    df = pd.read_excel(filename)

    return df

#####################
## Some cleanup functions

def remove_negative_masses(df):
    """
    'function' to return the dataframe with non-negative numbers
    """

    if (len(val(df.query('m1<0'))) >0) or (len(val(df.query('m2<0'))) >0):
        print("NEGATIVE MASS VALUES FOUND!!! CHECK THE DATAFRAME!!!")
    df = df.query('m1 >= 0')
    df = df.query('m2 >= 0')
    return df

###################################
# Adding VALUES                   # 
###################################

# Keep in mind; https://stackoverflow.com/questions/52673285/performance-of-pandas-apply-vs-np-vectorize-to-create-new-column-from-existing-c

#####################
## Chirpmass
def add_chirpmass_column(df, m1_name='m1', m2_name='m2', chirpmass_name='chirpmass'):
    """
    Function to add the column of chirp mass
    """

    df[chirpmass_name] = np.where(
        np.logical_and(
            df[m1_name] > 0,
            df[m2_name] > 0),
        calc_chirpmass(
            df[m1_name],
            df[m2_name]),
        0)

    return df

def add_pms_chirpmass_column(df):
    """
    Function to add the column of pms chirp mass
    """
    df['pms_chirpmass'] = np.where(
        np.logical_and(
            df['pms_mass_1'] > 0,
            df['pms_mass_2'] > 0),
        calc_chirpmass(
            df['pms_mass_1'],
            df['pms_mass_2']),
        0)
    return df

#####################
## total mass
def add_total_mass_column(df):
    df['total_mass'] = df['m1'] + df['m2']
    return df

def add_total_pms_mass_column(df):
    df['total_pms_mass'] = df['pms_mass_1'] + df['pms_mass_2']
    return df

#####################
## Mass ratios

def add_pms_q_1(df):
    """
    Function to add the column of initial (pms) mass ratio
    """
    df['pms_q_1'] = df['pms_mass_2']/df['pms_mass_1']
    return df

def add_remnant_q_1(df):
    """
    Function to add the column of initial (pms) mass ratio
    """
    df['remnant_q_1'] = np.where(
        np.logical_and(
            df['m1'] > 0,
            df['m2'] > 0
        ),
        df['m2']/df['m1'],
        0)
    return df



#####################
## Inspiral times
    

def add_inspiral_time_column(df):
    """
    Function to add a column of inspiral times to the dataframe
    """
    df['inspiral_time'] = np.where(
        np.logical_and(
            df['m1'] > 0, 
            df['m2'] > 0),
        calc_inspiral_time(
            df['m1'],
            df['m2'],
            df['orbital_period'],
            df['eccentricity']), 1e90)
    return df

#####################
## Inspiral times
def add_t_del_column(df):
    df['delay_time'] = df['time'] + df['inspiral_time']/pow(10, 6)
    return df

#####################
## merging time
def add_merge_within_hubble_time_column(df):
    """
    Function to add boolean values of whether the inspiral time happens witin the hubble time
    
    Semi useful function. this function is 
    """
    t_max = 13.7 * pow(10, 9)
    df['merge_within_hubble_time'] = np.where(
        np.logical_and(
            df['inspiral_time'] > 0, 
            df['inspiral_time'] < t_max - df['time'] * pow(10, 6)),
        True,
        False)
    return df


################## 
# Managing functions

def add_columns_df(df):
    """
    Function to add extra columns to the dataframe
    """

    # Calculate extra things on the dataset
    df = remove_negative_masses(df)     # Remove entries with negative masses

    # New columns (all functions based on numpy series)
    df = add_chirpmass_column(df)           # Add column with the chirpmasses
    df = add_pms_chirpmass_column(df)       # Add column with the pms chirpmasses
    df = add_total_pms_mass_column(df)  # Add column with the total pre main sequence mass
    df = add_total_mass_column(df)      # Add column with the total final mass
    df = add_pms_q_1(df)                # Add column for the initial mass ratio m2/m1
    df = add_remnant_q_1(df)            # add columns with the remnant mass ratio m2/m1

    df = add_inspiral_time_column(df)
    df = add_merge_within_hubble_time_column(df)
    df = add_t_del_column(df)

    df = df.drop('q_1', axis=1)    # Remove q_1 column

    return df

##################
# Statistic functions

def get_counts_and_probabilities(df, print_numbers=True):
    nsns = df.query('stellar_type_1==13 & stellar_type_2==13', inplace=False)
    bhns = df.query('(stellar_type_1==13 & stellar_type_2==14) or (stellar_type_1==14 & stellar_type_2==13)', inplace=False)
    bhbh = df.query('stellar_type_1==14 & stellar_type_2==14', inplace=False)

    # Get probabilities
    total_probability = df['probability'].sum()

    nsns_probability = nsns['probability'].sum()
    bhns_probability = bhns['probability'].sum()
    bhbh_probability = bhbh['probability'].sum()

    other_probability = total_probability - nsns_probability - bhns_probability - bhbh_probability

    # Get counts
    total_counts = len(df.index)

    nsns_counts = len(nsns.index)
    bhns_counts = len(bhns.index)
    bhbh_counts = len(bhbh.index)

    other_counts = total_counts - nsns_counts - bhns_counts - bhbh_counts

    # make dicts
    counts_dict = {
        'ns-ns': nsns_counts,
        'bhns': bhns_counts,
        'bh-bh': bhbh_counts,
        'non-dco': other_counts}
    prob_dict = {
        'ns-ns': nsns_probability,
        'bhns': bhns_probability,
        'bh-bh': bhbh_probability,
        'non-dco': other_probability
        }

    result_dict = {
        'counts': counts_dict, 
        'probabilities': prob_dict
    }


    if print_numbers: # print if necessary
        print('total amount: %d\tprobability: %f'%(total_counts, total_probability))

        print('nsns amount: %d\tprobability: %f'%(nsns_counts, nsns_probability))
        print('bhns amount: %d\tprobability: %f'%(bhns_counts, bhns_probability))
        print('bhbh amount: %d\tprobability: %f'%(bhbh_counts, bhbh_probability))

        print('other amount: %d\tprobability: %f'%(other_counts, other_probability))

    # Plot if necessary


    return total_counts, total_probability, result_dict




################## 
# Not yet processed.








# 
def add_lbv_df(df):
    """
    Function to add the column of whether the star went into an LBV phase
    """
    
    luminosity_list = df['luminosity'].values.tolist()
    radius_list = df['radius'].values.tolist()
    lbv_list = []
    for i in range(len(luminosity_list)):
        if (luminosity_list[i]>6*pow(10,5)) & (radius_list[i]*math.sqrt(luminosity_list[i])>pow(10,5)):
            lbv_list.append(1)
        else:
            lbv_list.append(0)
    lbv_df = pd.DataFrame(index=df.index, data=lbv_list, columns=['lbv_phase'])
    df = df.merge(lbv_df, how='outer', left_index=True, right_index=True)
    return df

####################

"""
TODO: stop using list in the merging columns but create the column 
immediatly and then update the values.
"""


########################################
# Set up and categorize totalDataFrame #  
########################################

def categorize_dataframe_indices(df):    
    """
    Evaluate the dataframe and split the data into different categories, based on queries
    Working only with index numbers. This is to reduce overhead, and make smart use of memory :P (my pc doesnt like this shit)
    """

    #Split the dataframe into common envelope mergers, and non common envelope mergers
    ce_mergers_indices, binaries_indices = split_dataframe_indices(df, 'stellar_type_1==15 | stellar_type_2==15')

    #Split binaries into those which are runaways and those which are 'stable'
    runaways_indices, stable_binaries_indices = split_dataframe_indices(df, 'eccentricity==-1', including_subset_indices=binaries_indices)

    #Split the 'stable' binaries dco systems into those who merge, and those who don't
    merging_dcos_indices, non_merging_dcos_indices = split_dataframe_indices(df, 'merge_within_hubble_time==True', including_subset_indices=stable_binaries_indices)

    # Split everything into different stellartypes
    bhbh_indices, non_bhbh_indices = split_dataframe_indices(df, 'stellar_type_1==14 & stellar_type_2==14', including_subset_indices=stable_binaries_indices)
    bhns_indices, non_bhns_indices = split_dataframe_indices(df, 'stellar_type_1==14 & stellar_type_2==13', including_subset_indices=non_bhbh_indices)
    nsbh_indices, non_nsbh_indices = split_dataframe_indices(df, 'stellar_type_1==13 & stellar_type_2==14', including_subset_indices=non_bhns_indices)
    nsns_indices, remaining_indices = split_dataframe_indices(df, 'stellar_type_1==13 & stellar_type_2==13', including_subset_indices=non_nsbh_indices)

    # Get set of merging DCOS
    merging_bhbh_indices, non_merging_bhbh_indices = split_dataframe_indices(df, 'merge_within_hubble_time==True', including_subset_indices=bhbh_indices)
    merging_bhns_indices, non_merging_bhns_indices = split_dataframe_indices(df, 'merge_within_hubble_time==True', including_subset_indices=bhns_indices)
    merging_nsbh_indices, non_merging_nsbh_indices = split_dataframe_indices(df, 'merge_within_hubble_time==True', including_subset_indices=nsbh_indices)
    merging_nsns_indices, non_merging_nsns_indices = split_dataframe_indices(df, 'merge_within_hubble_time==True', including_subset_indices=nsns_indices)

    categorized_dict  = {}

    categorized_dict['main_df'] = list(df.index.values)

    categorized_dict['ce_mergers'] = ce_mergers_indices
    categorized_dict['binaries'] = binaries_indices

    categorized_dict['runaways'] = runaways_indices
    categorized_dict['stable_binaries'] = stable_binaries_indices

    categorized_dict['merging_dcos'] = merging_dcos_indices
    categorized_dict['non_merging_dcos'] = non_merging_dcos_indices

    categorized_dict['bhbh'] = bhbh_indices
    categorized_dict['bhns'] = bhns_indices
    categorized_dict['nsbh'] = nsbh_indices
    categorized_dict['nsns'] = nsns_indices

    categorized_dict['non_bhbh'] = non_bhbh_indices
    categorized_dict['non_bhns'] = non_bhns_indices
    categorized_dict['non_nsbh'] = non_nsbh_indices
    categorized_dict['remaining'] = remaining_indices

    categorized_dict['merging_bhbh'] = merging_bhbh_indices
    categorized_dict['non_merging_bhbh'] = non_merging_bhbh_indices

    categorized_dict['merging_bhns'] = merging_bhns_indices
    categorized_dict['non_merging_bhns'] = non_merging_bhns_indices

    categorized_dict['merging_nsbh'] = merging_nsbh_indices
    categorized_dict['non_merging_nsbh'] = non_merging_nsbh_indices

    categorized_dict['merging_nsns'] = merging_nsns_indices
    categorized_dict['non_merging_nsns'] = non_merging_nsns_indices

    return categorized_dict

def categorize_binary_dataframe(df):
    df = add_columns_df(df)

    #Make a copy of the dataframe to keep this as the untouched parent
    main_df = df.copy()

    #Split the dataframe into common envelope mergers, and non common envelope mergers
    ce_mergers, binaries = split_dataframe(df, 'stellar_type_1==15 | stellar_type_2==15')

    #Split binaries into those which are runaways and those which are 'stable'
    runaways, stable_binaries = split_dataframe(binaries, 'eccentricity==-1')

    #Split the 'stable' binaries dco systems into those who merge, and those who don't
    merging_dcos, non_merging_dcos = split_dataframe(stable_binaries, 'merge_within_hubble_time==True')

    #Split everything into different stellartypes
    bhbh, non_bhbh = split_dataframe(stable_binaries, 'stellar_type_1==14 & stellar_type_2==14')
    bhns, non_bhns = split_dataframe(non_bhbh, 'stellar_type_1==14 & stellar_type_2==13')
    nsbh, non_nsbh = split_dataframe(non_bhns, 'stellar_type_1==13 & stellar_type_2==14')
    nsns, remaining = split_dataframe(non_nsbh, 'stellar_type_1==13 & stellar_type_2==13')

    merging_bhbh, non_merging_bhbh = split_dataframe(bhbh, 'merge_within_hubble_time==True')
    merging_bhns, non_merging_bhns = split_dataframe(bhns, 'merge_within_hubble_time==True')
    merging_nsbh, non_merging_nsbh = split_dataframe(nsbh, 'merge_within_hubble_time==True')
    merging_nsns, non_merging_nsns = split_dataframe(nsns, 'merge_within_hubble_time==True')

    categorized_dict  = {}
    categorized_dict['main_df'] = main_df

    categorized_dict['ce_mergers'] = ce_mergers
    categorized_dict['binaries'] = binaries

    categorized_dict['runaways'] = runaways
    categorized_dict['stable_binaries'] = stable_binaries

    categorized_dict['merging_dcos'] = merging_dcos
    categorized_dict['non_merging_dcos'] = non_merging_dcos

    categorized_dict['bhbh'] = bhbh
    categorized_dict['bhns'] = bhns
    categorized_dict['nsbh'] = nsbh
    categorized_dict['nsns'] = nsns

    categorized_dict['non_bhbh'] = non_bhbh
    categorized_dict['non_bhns'] = non_bhns
    categorized_dict['non_nsbh'] = non_nsbh
    categorized_dict['remaining'] = remaining

    categorized_dict['merging_bhbh'] = merging_bhbh
    categorized_dict['non_merging_bhbh'] = non_merging_bhbh

    categorized_dict['merging_bhns'] = merging_bhns
    categorized_dict['non_merging_bhns'] = non_merging_bhns

    categorized_dict['merging_nsbh'] = merging_nsbh
    categorized_dict['non_merging_nsbh'] = non_merging_nsbh

    categorized_dict['merging_nsns'] = merging_nsns
    categorized_dict['non_merging_nsns'] = non_merging_nsns
    return categorized_dict

###########################
# Statistics and analysis #
###########################

def print_dataframe_statistics(df, main_df, title):
    """
    Function to print some dataframe statistics
    """

    amt = df.shape[0]
    summed_prob = sum(val(df['probability']))
    main_summed_prob = sum(val(main_df['probability']))
    percent_of_total = 100*(summed_prob/main_summed_prob)
    print("{}".format(title))
    print("Total: {}\tTotal probability: {} ({}% of total probability)").format(amt, summed_prob, percent_of_total)


