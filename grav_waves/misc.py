STELLAR_TYPE_DICT = {
    0: "MS (M<0.7)",
    1: "MS (M>0.7)",
    2: "HG",
    3: "GB",
    4: "CHeb",
    5: "TPAGB",
    6: "HeMS",
    7: "HeMS",
    8: "HeHG",
    9: "HeGB",
    10: "HeWD",
    11: "COWD",
    12: "ONeWD",
    13: "NS",
    14: "BH",
    15: "MR"
}