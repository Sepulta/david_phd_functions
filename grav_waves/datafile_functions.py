"""
Functions for handling datafiles that come from the gravitational wave simulations
"""

import os
from grav_waves.run_population_scripts.functions import combine_resultfiles


def split_compact_objects_datafiles(full_file_path, remove_old_results=False):
    """
    Function to split the compact objects datafiles.

    These should contain ALL lines containing objects that are stellar_types=>13

    input is the full filename of the file that has to be split.

    Will create 5 files: bhbh.dat, bhns.dat, nsns.dat, other.dat combined_dco.dat
        the last one is the combination of all the DCO's (disregarding the 'other' data)

    TODO: make option to split into hdf5.
    """

    # 
    parentdir = os.path.dirname(full_file_path)
    if not os.path.isdir(parentdir):
        print("parentdir is not a valid directory")
        return None

    # Check if the actual main datafile exists
    if not os.path.isfile(full_file_path):
        print("File doesnt exist: {}".format(full_file_path))

        print("Attempting to combine all the chunks into {}".format(full_file_path))

        # Try to combine all the available chunks
        combine_resultfiles(
            parentdir,
            'compact_objects',
            'total_compact_objects.dat',
            check_duplicates_and_all_present=False
        )

        # check if succesful
        if os.path.isfile(full_file_path):
            print("Succesfully combined the chunks")

    # Set up flags to check if the dco types are found
    found_bhbh = False
    found_bhns = False
    found_nsns = False
    found_other = False

    # Create filenames and check if they exist already
    nsns_filename = os.path.join(parentdir, "nsns.dat")
    bhns_filename = os.path.join(parentdir, "bhns.dat")
    bhbh_filename = os.path.join(parentdir, "bhbh.dat")
    other_filename = os.path.join(parentdir, "other.dat")
    combined_dco_filename = os.path.join(parentdir, "combined_dco.dat")

    for filename in [nsns_filename, bhns_filename, bhbh_filename, other_filename, combined_dco_filename]:
        if os.path.isfile(filename):
            # Check if the file is empty:
            if os.stat(filename).st_size == 0:
                os.remove(filename)
            # If its not:
            else:
                print("File {} already exists from previous split".format(filename))
                raise ValueError

    # Open all the files
    nsns_file = open(nsns_filename, 'w')
    bhns_file = open(bhns_filename, 'w')
    bhbh_file = open(bhbh_filename, 'w')
    other_file = open(other_filename, 'w')
    combined_dco_file = open(combined_dco_filename, 'w')

    # open file
    with open(full_file_path) as f:
        # Get indices
        headerline = f.readline()
        headers = headerline.split()

        stellar_type_1_index = headers.index('stellar_type_1')
        stellar_type_2_index = headers.index('stellar_type_2')

        # Write headerline to split files
        for file in [nsns_file, bhns_file, bhbh_file, other_file, combined_dco_file]:
            file.write(headerline)

        # Loop over results:
        for line in f:
            split_line = line.strip().split()

            if ((split_line[stellar_type_1_index]=='14') and (split_line[stellar_type_2_index]=='14')):
                found_bhbh = True
                bhbh_file.write(line)
                combined_dco_file.write(line) # Add to the combined dco file
            elif ((split_line[stellar_type_1_index]=='13') and (split_line[stellar_type_2_index]=='13')):
                found_nsns = True
                nsns_file.write(line)
                combined_dco_file.write(line) # Add to the combined dco file
            elif (((split_line[stellar_type_1_index]=='13') and (split_line[stellar_type_2_index]=='14')) or ((split_line[stellar_type_1_index]=='14') and (split_line[stellar_type_2_index]=='13'))):
                found_bhns = True
                bhns_file.write(line)
                combined_dco_file.write(line) # Add to the combined dco file
            else:
                found_other = True
                other_file.write(line)

    # Close files
    for file in [nsns_file, bhns_file, bhbh_file, other_file, combined_dco_file]:
        file.close()

    # Incase none were found, remove the empty file again
    if not found_nsns:
        os.remove(nsns_filename)
    if not found_bhns:
        os.remove(bhns_filename)
    if not found_bhbh:
        os.remove(bhbh_filename)
    if not found_other:
        os.remove(other_filename)
    if (not found_nsns) and (not found_bhns) and (not found_bhbh):
        os.remove(combined_dco_filename)

# example_file = '/home/david/projects/binary_c_root/binary_c_python_scripts/grav_waves/gw_analysis/results_bigsim/DEFAULT/GW_z0.002/compact_objects.dat'
# split_compact_objects_datafiles(example_file)
