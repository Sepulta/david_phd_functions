"""
Functions to handle backups

TODO: add option to move the file into the backup folder that is stored within that main root
"""

import os
import shutil
import datetime
import tarfile

def backup_if_exists(directory, remove_old_directory_after_backup=False, store_in_backup_dir=True):
    """
    Function that checks whether a directory and its substructure exists, and makes a backup of that dir

    This backup is labeled with 'before_<time of backup>' to show that its a backup from material generated before that datetime
    """

    datetime_now = datetime.datetime.now()
    # print(datetime_now)

    datetime_now_format = datetime_now.strftime("%Y%m%d_%H%M%S")
    # print(datetime_now_format)

    # construct the filename
    if store_in_backup_dir:

        # Check if the directory argument ends with '/', if so, strip it
        if directory.endswith('/'):
            directory = directory[:-1]

        # Create names
        basename = os.path.basename(directory)
        dirname = os.path.dirname(directory)

        # Construct basename for file
        target_basename = "{}_before_{}.tar.gz".format(basename, datetime_now_format)

        # construct dirname and make if necessary
        target_dirname = os.path.join(dirname, '_backups')
        os.makedirs(target_dirname, exist_ok=True)

        # Construct target filename
        target_filename = os.path.join(target_dirname, target_basename)
    else:
        target_filename = "{}_before_{}.tar.gz".format(directory, datetime_now_format)
        # print(target_filename)

    # Check if the dir exists
    if os.path.isdir(directory):
        print("Found the target directory {}\n\tcreating a compressed file now: {}".format(directory, target_filename))
        # Create tarfile
        with tarfile.open(target_filename, "w:gz") as tar:
            tar.add(directory, arcname=os.path.basename(directory))

        # Remove dir structure if it exists
        if remove_old_directory_after_backup:
            print("Removing the original directory")
            shutil.rmtree(directory)
    else:
        print("Directory doesn't exist. Not doing anything")
# backup_if_exists("/home/david/Desktop/testbackup", remove_old_directory_after_backup=True)
