"""
Script containing functions to write pdfs

TODO: there is a problem with the bookmark function that we need to fix.
"""

import os
import fpdf
import copy
import fitz  # pip install pymupdf

from PyPDF2 import PdfReader

def get_bookmarks(filepath: str):
    # WARNING! One page can have multiple bookmarks!
    bookmarks = []
    with fitz.open(filepath) as doc:
        # toc = doc.getToC()  # [[lvl, title, page, …], …]
        toc = doc.get_toc()
        for level, title, page in toc:
            bookmarks.append({'title': title, 'level': level, 'page': page})
    return bookmarks

def generate_chapterpage_pdf(chaptername, output_name):
    """
    Function to generate a pdf containing some information about the dataset
    """

    # Write settings to pdf
    pdf = fpdf.FPDF(format='letter')
    pdf.add_page()
    pdf.set_font("Arial", size=24)
    pdf.write(10, '{}'.format(chaptername))
    pdf.ln()
    pdf.output(output_name)

def get_length_pdf(filename):
    """
    Function to get the pagecount of the pdf
    """

    if os.path.isfile(filename):
        with open(filename, 'rb') as pdf_file:
            pdf_reader = PdfReader(pdf_file)
            length_pdf = len(pdf_reader.pages)
    else:
        length_pdf = 0
    return length_pdf

def check_and_update_parent_list(parent_list, shift, current_level, bookmark):
    """
    Function to check the length of the parent list and update accordingly
    """

    # Make current bookmark available as possible parent
    if len(parent_list) + shift < current_level:
        parent_list.append(copy.deepcopy(bookmark))

        if len(parent_list) + shift < current_level:
            raise ValueError("issue with parent list length: current_level: {} len(parent_list): {}".format(current_level, len(parent_list)))

    # If we jump back to a higher level, we need to remove the previous parent elements
    elif len(parent_list) + shift > current_level:
        parent_list = parent_list[:current_level]
        parent_list.append(copy.deepcopy(bookmark))

    return parent_list

def add_pdf_and_bookmark(merger, pdf_filename, page_number, bookmark_text=None, add_source_bookmarks=False):
    """
    Function to wrap bookmark stuff
    """

    # Add into merger
    merger.append(pdf_filename)

    # Add bookmark
    if bookmark_text and isinstance(bookmark_text, str):
        parent = merger.add_outline_item(title=bookmark_text, pagenum=page_number)

        # Construct list of parent bookmarks
        parent_list = [parent]

        if add_source_bookmarks:
            #  get the bookmarks that the source has:
            source_bookmarks = get_bookmarks(pdf_filename)

            if source_bookmarks:

                # Go over the bookmarks
                for bookmark_dict in source_bookmarks:
                    current_level = bookmark_dict['level']

                    # In the case that we are adding source bookmarks, we need
                    bookmark = merger.addBookmark(
                        bookmark_dict['title'],
                        page_number + bookmark_dict['page'] - 1,
                        parent=parent_list[current_level-1]
                    ) # add child bookmark

                    parent_list = check_and_update_parent_list(parent_list, -1, current_level, bookmark)

    else:
        # Construct list of parent bookmarks
        parent_list = []

        if add_source_bookmarks:
            #  get the bookmarks that the source has:
            source_bookmarks = get_bookmarks(pdf_filename)

            if source_bookmarks:
                # Go over the bookmarks
                for bookmark_dict in source_bookmarks:
                    current_level = bookmark_dict['level']

                    # In the case that we are adding source bookmarks, we need
                    if current_level == 1:
                        bookmark = merger.add_outline_item(
                            bookmark_dict['title'],
                            page_number + bookmark_dict['page'] - 1,
                        ) # add child bookmark
                    else:
                        bookmark = merger.addBookmark(
                            bookmark_dict['title'],
                            page_number + bookmark_dict['page'] - 1,
                            parent=parent_list[current_level-1]
                        ) # add child bookmark

                    parent_list = check_and_update_parent_list(parent_list, 0, current_level, bookmark)

    # Inspect the pagelength of the pdf
    length_pdf = get_length_pdf(pdf_filename)
    page_number += length_pdf

    return merger, page_number
