"""
This file contains a set of functions to generate bins from data
"""

import numpy as np

#####
# Some useful numpy functions
def create_centers_from_bins(bins):
    """
    Function to create centers from bin edges
    """

    return (bins[1:] + bins[:-1]) / 2


def create_bins_from_centers(centers):
    """
    Function to create a set of bin edges from a set of bin centers. Assumes the two endpoints have the same binwidth as their neighbours
    """

    # Create bin edges minus the outer two
    bin_edges = (centers[1:] + centers[:-1]) / 2

    # Add to left
    bin_edges = np.append(np.array(bin_edges[0] - np.diff(centers)[0]), bin_edges)

    # Add to right
    bin_edges = np.append(bin_edges, np.array(bin_edges[-1] + np.diff(centers)[-1]))

    return bin_edges

def create_bin_dict(array):
    """
    Function to create a dictionary with bin-info given an array of regularly spaced data
    """

    # sort and make unique
    unique_array = np.unique(array)
    sorted_array = np.sort(unique_array)

    # create bincenters
    bin_centers = create_centers_from_bins(bins=sorted_array)
    bin_edges = create_bins_from_centers(centers=bin_centers)

    return {"centers": bin_centers, 'edges': bin_edges}