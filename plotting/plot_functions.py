import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib
import math
import numpy as np

from binarycpython.utils.stellar_types import stellar_type_dict_short



import astropy.units as u
import astropy.constants as const
######################################################
# Sorted
######################################################

# Triangle plot layout
def make_triangle_grid_four():
    """
    Function that provides the setup for a four by four triangle plot
    
    return fig
    """

    # create grid
    fig = plt.figure()

    gs = matplotlib.gridspec.GridSpec(4,4, figure=fig)
    # gs.update(wspace=0.5, hspace=0.5)

    ax11 = plt.subplot(gs[0:1,0:1])    
    ax12 = plt.subplot(gs[0:1,1:2])
    ax13 = plt.subplot(gs[0:1,2:3])
    ax14 = plt.subplot(gs[0:1,3:4])
    ax22 = plt.subplot(gs[1:2,1:2])    
    ax23 = plt.subplot(gs[1:2,2:3])
    ax24 = plt.subplot(gs[1:2,3:4])    
    ax33 = plt.subplot(gs[2:3,2:3])
    ax34 = plt.subplot(gs[2:3,3:4])
    ax44 = plt.subplot(gs[3:4,3:4])

    # Group the axes
    axes = [ax11,ax12,ax13,ax14, #ax15,
                 ax22,ax23,ax24, #ax25,
                      ax33,ax34, #ax35,
                           ax44, #ax45,ax55
    ]  

    ax_offdiag = [ax12,ax13, ax14,
                  ax23, ax24,ax34]#ax45,ax25,ax35]

    ax_ondiag = [ax11,ax22,ax33,ax44] #,ax55]

    # Remove the tick labels
    for ax in ax_offdiag:
        ax.set_xticklabels([])
        ax.set_yticklabels([])

    return fig

def make_triangle_grid_three():
    """
    Function that provides the setup for a three by three triangle plot
    
    return fig
    """


    # create grid
    fig = plt.figure()

    gs = matplotlib.gridspec.GridSpec(3, 3, figure=fig)
    # gs.update(wspace=0.5, hspace=0.5)

    ax11 = plt.subplot(gs[0:1,0:1])    
    ax12 = plt.subplot(gs[0:1,1:2])
    ax13 = plt.subplot(gs[0:1,2:3])

    ax22 = plt.subplot(gs[1:2,1:2])
    ax23 = plt.subplot(gs[1:2,2:3])

    ax33 = plt.subplot(gs[2:3, 2:3])

    # Group the axes
    axes = [ax11,ax12,ax13,
                 ax22,ax23,
                      ax33,
    ]

    ax_offdiag = [ax12, ax13,
                        ax23, ]

    ax_ondiag = [ax11,ax22,ax33]

    # Remove the tick labels
    for ax in ax_offdiag:
        ax.set_xticklabels([])
        ax.set_yticklabels([])

    return fig

def make_triangle_grid_two():
    """
    Function that provides the setup for a three by three triangle plot
    
    return fig
    """


    # create grid
    fig = plt.figure()

    gs = matplotlib.gridspec.GridSpec(2, 2, figure=fig)
    # gs.update(wspace=0.5, hspace=0.5)

    ax11 = plt.subplot(gs[0:1,0:1])    
    ax12 = plt.subplot(gs[0:1,1:2])

    ax22 = plt.subplot(gs[1:2, 1:2])

    # Group the axes
    axes = [ax11,ax12,
                 ax22,
            ]

    ax_offdiag = [ax12]

    ax_ondiag = [ax11,ax22]

    # Remove the tick labels
    for ax in ax_offdiag:
        ax.set_xticklabels([])
        ax.set_yticklabels([])

    return fig


#######################
# Functions to plot quantities
# TODO: add more plots: rotation rates. 
# TODO: add information about required fields in the docstring

def add_stellar_types_bar(df, fig, ax_index=-1, only_colorbar=False):
    """
    Function to add stellar types bar to a plot.
    It wont shape the figure in a nice way, so please assign a size to it 
    
    ax_index = index of the ax 
    """

    #####
    # Create colors
    colors=plt.cm.get_cmap('viridis', 16).colors

    new_ticklabels = ["{} ({})".format(stellar_type_dict_short[i], i) for i in range(0, 16)]


    # Create colorbar
    norm = mpl.colors.Normalize(vmin=-0.5, vmax=16-0.5)
    sm = plt.cm.ScalarMappable(cmap=plt.cm.get_cmap('viridis', 16), norm=norm)
    sm.set_array([])
    cbar = fig.colorbar(sm, ax=fig.axes, shrink=0.95, ticks=np.linspace(0,16,17))
    cbar.ax.set_yticklabels(new_ticklabels)

    if not only_colorbar:
        #####
        # Get unique stellar types and get the times at which they change
        unique_stellar_types_1 = df['stellar_type_1'].unique()
        unique_stellar_types_2 = df['stellar_type_2'].unique()

        stellar_type_1_time_dict = {}
        for stellar_type in unique_stellar_types_1:
            min_time = df[df.stellar_type_1==stellar_type]['time'].min()
            max_time = df[df.stellar_type_1==stellar_type]['time'].max()
            stellar_type_1_time_dict[stellar_type] = [min_time, max_time]

        stellar_type_2_time_dict = {}
        for stellar_type in unique_stellar_types_2:
            min_time = df[df.stellar_type_2==stellar_type]['time'].min()
            max_time = df[df.stellar_type_2==stellar_type]['time'].max()
            stellar_type_2_time_dict[stellar_type] = [min_time, max_time]

    #####
    # Create area plots
        for stellar_type in stellar_type_1_time_dict.keys():
            entry = stellar_type_1_time_dict[stellar_type]
            fig.axes[ax_index].fill_between([entry[0], entry[1]], [0,0], [1,1], color=colors[stellar_type])

        for stellar_type in stellar_type_2_time_dict.keys():
            entry = stellar_type_2_time_dict[stellar_type]
            fig.axes[ax_index].fill_between([entry[0], entry[1]], [1,1], [2,2], color=colors[stellar_type])

        # Some make up
        fig.axes[ax_index].set_ylim(0, 2)
        fig.axes[ax_index].set_ylabel('Stellar types')
        fig.axes[ax_index].set_xlabel('Time (Myr)')


    # ratio = 0.1
    # xleft, xright = fig.axes[ax_index].get_xlim()
    # ybottom, ytop = fig.axes[ax_index].get_ylim()
    # # the abs method is used to make sure that all numbers are positive
    # # because x and y axis of an axes maybe inversed.
    # fig.axes[ax_index].set_aspect(abs((xright-xleft)/(ybottom-ytop))*ratio)

    return fig, colors

def plot_angmom_derivatives(df, show_stellar_types=False, show_plot=False):
    """
    Function to plot angular momenta of a binary system.

    Plots the following quantities:
        - The angular momentum derivatives. Takes all quantities, checks if there are any values in them and then plot them
    """

    if show_stellar_types:
        fig, ax = plt.subplots(ncols=1, nrows=2, figsize=(20, 10))
        fig.subplots_adjust(hspace=0)
    else:
        fig, ax = plt.subplots(ncols=1, nrows=2, figsize=(20, 10), sharex=True)
        fig.subplots_adjust(hspace=0)

        # Angmom derivatives
        # Star 1 angmom gain
        ANGMOM_GAIN_1 = ['DERIVATIVE_STELLAR_ANGMOM_WIND_GAIN_1', 'DERIVATIVE_STELLAR_ANGMOM_RLOF_GAIN_1', 'DERIVATIVE_STELLAR_ANGMOM_TIDES_GAIN_1', 'DERIVATIVE_STELLAR_ANGMOM_DECRETION_DISC_GAIN_1', 'DERIVATIVE_STELLAR_ANGMOM_CBDISC_GAIN_1', 'DERIVATIVE_STELLAR_ANGMOM_CORE_ENVELOPE_COUPLING_GAIN_1']

        # Star 2 angmom gain
        ANGMOM_GAIN_2 = ['DERIVATIVE_STELLAR_ANGMOM_WIND_GAIN_2', 'DERIVATIVE_STELLAR_ANGMOM_RLOF_GAIN_2', 'DERIVATIVE_STELLAR_ANGMOM_TIDES_GAIN_2', 'DERIVATIVE_STELLAR_ANGMOM_DECRETION_DISC_GAIN_2', 'DERIVATIVE_STELLAR_ANGMOM_CBDISC_GAIN_2', 'DERIVATIVE_STELLAR_ANGMOM_CORE_ENVELOPE_COUPLING_GAIN_2']

        # Star 1 angmom loss
        ANGMOM_LOSS_1 = ['DERIVATIVE_STELLAR_ANGMOM_WIND_LOSS_1', 'DERIVATIVE_STELLAR_ANGMOM_RLOF_LOSS_1', 'DERIVATIVE_STELLAR_ANGMOM_TIDES_LOSS_1', 'DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING_1', 'DERIVATIVE_STELLAR_ANGMOM_DECRETION_DISC_LOSS_1', 'DERIVATIVE_STELLAR_ANGMOM_NOVA_1', 'DERIVATIVE_STELLAR_ANGMOM_NONCONSERVATIVE_LOSS_1', 'DERIVATIVE_STELLAR_ANGMOM_CORE_ENVELOPE_COUPLING_LOSS_1']

        # Star 2 angmom loss
        ANGMOM_LOSS_2 = ['DERIVATIVE_STELLAR_ANGMOM_WIND_LOSS_2', 'DERIVATIVE_STELLAR_ANGMOM_RLOF_LOSS_2', 'DERIVATIVE_STELLAR_ANGMOM_TIDES_LOSS_2', 'DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING_2', 'DERIVATIVE_STELLAR_ANGMOM_DECRETION_DISC_LOSS_2', 'DERIVATIVE_STELLAR_ANGMOM_NOVA_2', 'DERIVATIVE_STELLAR_ANGMOM_NONCONSERVATIVE_LOSS_2', 'DERIVATIVE_STELLAR_ANGMOM_CORE_ENVELOPE_COUPLING_LOSS_2']



    for AM_deriv in ANGMOM_GAIN_1:
        if not (df[AM_deriv] == 0).all():
            fig.axes[0].plot(df['time'], df[AM_deriv], label=AM_deriv)

    for AM_deriv in ANGMOM_LOSS_1:
        if not (df[AM_deriv] == 0).all():
            # Multiply by -1 to have everything on the same sign
            fig.axes[0].plot(df['time'], df[AM_deriv] * -1, label=AM_deriv, linestyle='--', alpha=0.7)

    for AM_deriv in ANGMOM_GAIN_2:
        if not (df[AM_deriv] == 0).all():
            fig.axes[1].plot(df['time'], df[AM_deriv], label=AM_deriv)

    for AM_deriv in ANGMOM_LOSS_2:
        if not (df[AM_deriv] == 0).all():
            # Multiply by -1 to have everything on the same sign
            fig.axes[1].plot(df['time'], df[AM_deriv] * -1, label=AM_deriv, linestyle='--',  alpha=0.7)

    # TODO: make ticks of 10-3 etc work better. 

    # TODO: Update this for when adding the stellar types 
    # sc2 = ax.scatter(df_real['time'], (df_real['orbital_angular_momentum'])*0.9, c=df_real['stellar_type_2'], s=100, cmap=CMAP, label='star 1')
    # fig.colorbar(sc1, ax=ax)
    # fig.colorbar(sc2, ax=ax)
    # sc1.set_zorder(20)
    # sc2.set_zorder(20)

    # Make up
    # fig.axes[0].set_yscale('symlog')
    # fig.axes[1].set_yscale('symlog')

    fig.axes[0].set_yscale('log')
    fig.axes[1].set_yscale('log')

    fig.axes[0].legend(loc='best')
    fig.axes[1].legend(loc='best')

    fig.axes[0].set_title('Stellar system orbital angular momentum evolution')
    fig.axes[0].set_ylabel(r'dM/dt ($M_{\odot}$)')
    fig.axes[1].set_ylabel(r'dM/dt ($M_{\odot}$)')

    fig.axes[1].set_xlabel(r'Time (Myr)')

    # Show or return
    if show_plot:
        plt.show()
    else:
        return fig

def plot_mass_derivatives(df, show_stellar_types=False, show_plot=False):
    """
    Function to plot angular momenta of a binary system.

    Plots the following quantities:
        - The mass derivatives. Takes all quantities, checks if there are any values in them and then plot them
    """

    if show_stellar_types:
        fig, ax = plt.subplots(ncols=1, nrows=2, figsize=(20, 10))
        fig.subplots_adjust(hspace=0)
    else:
        fig, ax = plt.subplots(ncols=1, nrows=2, figsize=(20, 10), sharex=True)
        fig.subplots_adjust(hspace=0)

    MASS_GAIN_1 = ['DERIVATIVE_STELLAR_MASS_WIND_GAIN_1', 'DERIVATIVE_STELLAR_MASS_RLOF_GAIN_1', 'DERIVATIVE_STELLAR_MASS_DISC_GAIN_1', 'DERIVATIVE_STELLAR_MASS_CBDISC_GAIN_1', 'DERIVATIVE_STELLAR_MASS_NOVA_GAIN_1', 'DERIVATIVE_STELLAR_MASS_ARTIFICIAL_GAIN_1']

    MASS_LOSS_1 = ['DERIVATIVE_STELLAR_MASS_WIND_LOSS_1', 'DERIVATIVE_STELLAR_MASS_RLOF_LOSS_1', 'DERIVATIVE_STELLAR_MASS_DISC_LOSS_1', 'DERIVATIVE_STELLAR_MASS_DECRETION_DISC_1', 'DERIVATIVE_STELLAR_MASS_NONCONSERVATIVE_LOSS_1', 'DERIVATIVE_STELLAR_MASS_NOVA_LOSS_1', 'DERIVATIVE_STELLAR_MASS_ARTIFICIAL_LOSS_1']

    MASS_GAIN_2 = ['DERIVATIVE_STELLAR_MASS_WIND_GAIN_2', 'DERIVATIVE_STELLAR_MASS_RLOF_GAIN_2', 'DERIVATIVE_STELLAR_MASS_DISC_GAIN_2', 'DERIVATIVE_STELLAR_MASS_CBDISC_GAIN_2', 'DERIVATIVE_STELLAR_MASS_NOVA_GAIN_2', 'DERIVATIVE_STELLAR_MASS_ARTIFICIAL_GAIN_2']

    MASS_LOSS_2 = ['DERIVATIVE_STELLAR_MASS_WIND_LOSS_2', 'DERIVATIVE_STELLAR_MASS_RLOF_LOSS_2', 'DERIVATIVE_STELLAR_MASS_DISC_LOSS_2', 'DERIVATIVE_STELLAR_MASS_DECRETION_DISC_2', 'DERIVATIVE_STELLAR_MASS_NONCONSERVATIVE_LOSS_2', 'DERIVATIVE_STELLAR_MASS_NOVA_LOSS_2', 'DERIVATIVE_STELLAR_MASS_ARTIFICIAL_LOSS_2']

    for MT_deriv in MASS_GAIN_1:
        if not (df[MT_deriv] == 0).all():
            fig.axes[0].plot(df['time'], df[MT_deriv], label=MT_deriv)

    for MT_deriv in MASS_LOSS_1:
        if not (df[MT_deriv] == 0).all():
            # Multiply by -1 to have everything on the same sign
            fig.axes[0].plot(df['time'], df[MT_deriv] * -1, label=MT_deriv, linestyle='--', alpha=0.7)

    for MT_deriv in MASS_GAIN_2:
        if not (df[MT_deriv] == 0).all():
            fig.axes[1].plot(df['time'], df[MT_deriv], label=MT_deriv)

    for MT_deriv in MASS_LOSS_2:
        if not (df[MT_deriv] == 0).all():
            # Multiply by -1 to have everything on the same sign
            fig.axes[1].plot(df['time'], df[MT_deriv] * -1, label=MT_deriv, linestyle='--',  alpha=0.7)

    # TODO: make ticks of 10-3 etc work better. 

    # TODO: Update this for when adding the stellar types 
    # sc2 = ax.scatter(df_real['time'], (df_real['orbital_angular_momentum'])*0.9, c=df_real['stellar_type_2'], s=100, cmap=CMAP, label='star 1')
    # fig.colorbar(sc1, ax=ax)
    # fig.colorbar(sc2, ax=ax)
    # sc1.set_zorder(20)
    # sc2.set_zorder(20)

    # Make up
    fig.axes[0].set_yscale('log')
    fig.axes[1].set_yscale('log')

    fig.axes[0].legend(loc='best')
    fig.axes[1].legend(loc='best')

    fig.axes[0].set_title('Stellar system orbital angular momentum evolution')
    fig.axes[0].set_ylabel(r'dM/dt ($M_{\odot}$)')
    fig.axes[1].set_ylabel(r'dM/dt ($M_{\odot}$)')

    fig.axes[1].set_xlabel(r'Time (Myr)')

    # Show or return
    if show_plot:
        plt.show()
    else:
        return fig

def plot_rotation_periods(df, show_stellar_types=False, show_plot=False):
    """
    Function to plot the rotation periods of the stars and the orbit

    Plots the following quantities (in days):
        - rotation period of star 1 
        - rotation period of star 2
        - critical rotation period of star 1
        - critical rotation period of star 2
        - Orbital period
    """

    if show_stellar_types:
        fig, ax = plt.subplots(ncols=1, nrows=2, figsize=(20, 10), sharex=True)
        fig.subplots_adjust(hspace=0)
        fig = add_stellar_types_bar(df, fig, ax_index=-1)
    else:
        fig, ax = plt.subplots(ncols=1, nrows=1, figsize=(20, 10))

    # 
    df['orbital_period_days'] = df['orbital_period'] * 365
    df['period_1'] = 365 * (2 * math.pi)/df['omega_1']
    df['period_2'] = 365 * (2 * math.pi)/df['omega_2']

    df['crit_period_1'] = 365 * (2 * math.pi)/df['omega_crit_1']
    df['crit_period_2'] = 365 * (2 * math.pi)/df['omega_crit_2']

    # plot data
    fig.axes[0].plot(df['time'], df['orbital_period_days'], label='Orbital period')
    fig.axes[0].plot(df['time'], df['period_1'], label='Period star 1')
    fig.axes[0].plot(df['time'], df['period_2'], label='Period star 2')

    fig.axes[0].plot(df['time'], df['crit_period_1'], label='Critical period star 1', linestyle='--', alpha=0.5)
    fig.axes[0].plot(df['time'], df['crit_period_2'], label='Critical period star 2', linestyle='--', alpha=0.5)

    # Make up
    fig.axes[0].set_yscale('log')
    fig.axes[0].legend(loc='best')

    fig.axes[0].set_title('Periods evolution')
    fig.axes[0].set_ylabel(r'Period (days)')
    fig.axes[0].set_xlabel(r'Time (Myr)')

    # if show_stellar_types:
    #     fig = add_stellar_types_bar(df, fig, ax_index=-1)

    # Show or return
    if show_plot:
        plt.show()
    else:
        return fig

def plot_angular_momenta(df, show_stellar_types=False, show_plot=False):
    """
    Function to plot angular momenta of a binary system.

    Plots the following quantities:
        - angular momentum of star 1
        - angular momentum of star 2
        - orbital angular momentum
    """

    if show_stellar_types:
        fig, ax = plt.subplots(ncols=1, nrows=2, figsize=(20, 10))
        fig.subplots_adjust(hspace=0)
    else:
        fig, ax = plt.subplots(ncols=1, nrows=1, figsize=(20, 10))

    # plot data
    ax.plot(df['time'], df['orbital_angular_momentum'], label='Orbital angular momentum')
    ax.plot(df['time'], df['angular_momentum_1'], label='Angular momentum star 1')
    ax.plot(df['time'], df['angular_momentum_2'], label='Angular momentum star 2')

    # TODO: Update this for when adding the stellar types 
    # sc2 = ax.scatter(df_real['time'], (df_real['orbital_angular_momentum'])*0.9, c=df_real['stellar_type_2'], s=100, cmap=CMAP, label='star 1')
    # fig.colorbar(sc1, ax=ax)
    # fig.colorbar(sc2, ax=ax)
    # sc1.set_zorder(20)
    # sc2.set_zorder(20)

    # Make up
    ax.set_yscale('log')
    ax.legend(loc='best')

    ax.set_title('Stellar system orbital angular momentum evolution')
    ax.set_ylabel(r'Angular momentum')
    ax.set_xlabel(r'Time (Myr)')

    # Show or return
    if show_plot:
        plt.show()
    else:
        return fig














############################
# Unsorted
############################
def make_triangle_grid(df, columns_list, logplot_density, axes_labels, save_file):
    """
    Add setting par to logplot 
    """
    
    # create grid
    fig = plt.figure(figsize=(18., 18.))
    
    gs = mpl.gridspec.GridSpec(400,400)
    
    ax11 = plt.subplot(gs[0:100,0:100])    
    ax12 = plt.subplot(gs[0:100,100:200])
    ax13 = plt.subplot(gs[0:100,200:300])
    ax14 = plt.subplot(gs[0:100,300:400])
    ax22 = plt.subplot(gs[100:200,100:200])    
    ax23 = plt.subplot(gs[100:200,200:300])
    ax24 = plt.subplot(gs[100:200,300:400])    
    ax33 = plt.subplot(gs[200:300,200:300])
    ax34 = plt.subplot(gs[200:300,300:400])
    ax44 = plt.subplot(gs[300:400,300:400])
    
    axes = [ax11,ax12,ax13,ax14, #ax15,
            ax22,ax23,ax24, #ax25,
            ax33, ax34, #ax35,
            ax44,#ax45,ax55
    ]
    
    ax_offdiag = [ax12,ax13, ax14,
                  ax23, ax24,ax34]#ax45,ax25,ax35]
    
    ax_ondiag = [ax11,ax22,ax33,ax44] #,ax55]
    
    ax_semidiag = [ax12, ax23, ax34]

    
    #################################################################################################### plotting below
    # generic parameters
    BB = 50 # number of bins
    CMAP = plt.get_cmap('viridis')

    ################################################################################# plots on diagonal

#     ax11.set_xlim(5.5,7)#(0,80)
    ax11.hist(df[columns_list[0]], bins=BB, weights=df['probability'])
    ax11.set_xlabel(axes_labels[0])
#     ax11.set_xlabel("$v_\mathrm{dis} \ [\mathrm{km \ s^{-1}}]$",fontsize=30)

#     #ax22.set_xlim(1.5,3.5)
#     ax22.set_xlim(min(np.log10(Pprev)),max(np.log10(Pprev)))
    ax22.hist(df[columns_list[1]], bins=BB, weights=df['probability'])
    ax22.set_xlabel(axes_labels[1])
#     ax22.set_xlabel("$\log_{10}(P_\mathrm{pre-SN}/\mathrm{days})$",fontsize=30)
    
#     ax33.set_xlim(1,6) 
    ax33.hist(df[columns_list[2]], bins=BB, weights=df['probability'])
    ax33.set_xlabel(axes_labels[2])
#     ax33.set_xlabel("$\log_{10}(P_\mathrm{ZAMS}/\mathrm{days})$",fontsize=30)

#     ax44.set_xlim(0.1,1)
    ax44.hist(df[columns_list[3]], bins=BB, weights=df['probability'])
#     ax44.set_xlabel("$q_\mathrm{ZAMS} \ [M_\odot]$",fontsize=30)
    ax44.set_xlabel(axes_labels[3])

    
    
    ################################################################################# plots off diagonal

    ### 12
    x1,x2 = ax22.get_xlim()
    y1,y2 = ax11.get_xlim()
    X,Y,mat = make2Dmap(df[columns_list[1]], df[columns_list[0]], df['probability'], x1, x2, y1, y2, BB)
    if logplot_density:
        minimal_value = np.min(mat[np.nonzero(mat)]) #excluding zeros
        ax12.pcolor(X, Y, mat, norm=LogNorm(vmin=minimal_value, vmax=mat.max()), cmap=CMAP)
    else:
        ax12.pcolor(X,Y,mat,norm=None,cmap=CMAP)
    
    ###  13
    x1,x2 = ax33.get_xlim()
    y1,y2 = ax11.get_xlim()
    X,Y,mat = make2Dmap(df[columns_list[2]], df[columns_list[0]], df['probability'], x1, x2, y1, y2, BB)
    if logplot_density:
        minimal_value = np.min(mat[np.nonzero(mat)]) #excluding zeros
        ax13.pcolor(X, Y, mat, norm=LogNorm(vmin=minimal_value, vmax=mat.max()), cmap=CMAP)
    else:
        ax13.pcolor(X,Y,mat,norm=None,cmap=CMAP)

    ### 14
    x1,x2 = ax44.get_xlim()
    y1,y2 = ax11.get_xlim()
    X,Y,mat = make2Dmap(df[columns_list[3]], df[columns_list[0]], df['probability'], x1, x2, y1, y2, BB)
    if logplot_density:
        minimal_value = np.min(mat[np.nonzero(mat)]) #excluding zeros
        ax14.pcolor(X, Y, mat, norm=LogNorm(vmin=minimal_value, vmax=mat.max()), cmap=CMAP)
    else:
        ax14.pcolor(X,Y,mat,norm=None,cmap=CMAP)

    ################################################## end line 1

    ### 23
    x1,x2 = ax33.get_xlim()
    y1,y2 = ax22.get_xlim()
    X,Y,mat = make2Dmap(df[columns_list[2]], df[columns_list[1]], df['probability'], x1, x2, y1, y2, BB)
    if logplot_density:
        minimal_value = np.min(mat[np.nonzero(mat)]) #excluding zeros
        ax23.pcolor(X, Y, mat, norm=LogNorm(vmin=minimal_value, vmax=mat.max()), cmap=CMAP)
    else:
        ax23.pcolor(X,Y,mat,norm=None,cmap=CMAP)
    
        
    ### 24
    x1,x2 = ax44.get_xlim()
    y1,y2 = ax22.get_xlim()
    X,Y,mat = make2Dmap(df[columns_list[3]], df[columns_list[1]], df['probability'], x1, x2, y1, y2, BB)
    if logplot_density:
        minimal_value = np.min(mat[np.nonzero(mat)]) #excluding zeros
        ax24.pcolor(X, Y, mat, norm=LogNorm(vmin=minimal_value, vmax=mat.max()), cmap=CMAP)
    else:
        ax24.pcolor(X,Y,mat,norm=None,cmap=CMAP)

    ####################################################### end line 2

    ### 34
    x1,x2 = ax44.get_xlim()
    y1,y2 = ax33.get_xlim()
    X,Y,mat = make2Dmap(df[columns_list[3]], df[columns_list[2]], df['probability'], x1, x2, y1, y2, BB)
    if logplot_density:
        minimal_value = np.min(mat[np.nonzero(mat)]) #excluding zeros
        ax34.pcolor(X, Y, mat, norm=LogNorm(vmin=minimal_value, vmax=mat.max()), cmap=CMAP)
    else:
        minimal_value = np.min(mat[np.nonzero(mat)]) #excluding zeros

        ax34.pcolor(X,Y,mat,norm=None,cmap=CMAP)
    
    ################################################################################ beautification
#     for ax in axes:
#         ax.spines['left'].set_linewidth(2)  
#         ax.spines['top'].set_linewidth(2)   
#         ax.spines['bottom'].set_linewidth(2)
#         ax.spines['right'].set_linewidth(2) 

#         ax.tick_params(axis='both', which='major', width=2,
#                        length=12, pad=10, labelsize=30)
#         ax.tick_params(axis='both', which='minor', width=2,
#                    length=6, pad=10)
    
#     for ax in ax_offdiag:
#         # ax.set_yticklabels([])
#         # ax.set_xticklabels([])
#         ax.tick_params(axis='both', which='major', width=2,
#                        length=12, pad=10, labelsize=30,color='w')
#         ax.tick_params(axis='both', which='minor', width=2,
#                        length=6, pad=10, color='w')

#         ax.spines['left'].set_color('w')  
#         ax.spines['top'].set_color('w')   
#         ax.spines['bottom'].set_color('w')
#         ax.spines['right'].set_color('w') 
#     for ax in ax_ondiag:
#         ax.set_yticklabels([])
        
    plt.tight_layout()
    plt.savefig(save_file, bbox_inches='tight')
    #gs.update(wspace=0,hspace=0)# top=1.1)

# Unsorted





























def density_plot(df, parameter_1, parameter_2, bins, logplot=False, plotkwargs=None):
    """
    Function to make a density plot using a 2d map function
    """

    X, Y, mat = make2Dmap(
            x=df[parameter_1], 
            y=df[parameter_2], 
            Prob=df['probability'],
            x1=min(df[parameter_1]),
            x2=max(df[parameter_1]),
            y1=min(df[parameter_2]),
            y2=max(df[parameter_2]),
            bins=bins)
    if logplot:
        minimal_value = np.min(mat[np.nonzero(mat)]) #excluding zeros
        plt.pcolor(X, Y, mat, norm=LogNorm(vmin=minimal_value, vmax=mat.max()), cmap=CMAP)
    else:
        plt.pcolor(X, Y, mat, norm=None, cmap=CMAP)
    plt.xlabel(parameter_1.title().replace('_',' '))
    plt.ylabel(parameter_2.title().replace('_',' '))
    plt.xlim(min(X), max(X))
    plt.ylim(min(Y), max(Y))
    plt.colorbar()
    return plt

def density_plot_ax(df, ax, parameter_1, parameter_2, bins, logplot=False, plotkwargs=None):
    """
    Function to make a density plot as an ax object
    """

    X, Y, mat = make2Dmap(
            x=df[parameter_1], 
            y=df[parameter_2], 
            Prob=df['probability'],
            x1=min(df[parameter_1]),
            x2=max(df[parameter_1]),
            y1=min(df[parameter_2]),
            y2=max(df[parameter_2]),
            bins=bins)
    if logplot:
        minimal_value = np.min(mat[np.nonzero(mat)]) #excluding zeros
        ax.pcolor(X, Y, mat, norm=LogNorm(vmin=minimal_value, vmax=mat.max()), cmap=CMAP)
    else:
        ax.pcolor(X, Y, mat, norm=None, cmap=CMAP)
    ax.set_xlabel(parameter_1.title().replace('_',' '))
    ax.set_ylabel(parameter_2.title().replace('_',' '))
    ax.set_xlim(min(X), max(X))
    ax.set_ylim(min(Y), max(Y))
    #ax.colorbar()
    return ax

def auto_plot_all(df_list, excluded_headers_list, plot_type_dict=None):
    """
    Make the df input so that i'm able to input more dfs, in order to overlay plots automatically
    """
    
    all_columns = df_list[0].columns.values.tolist()
    filtered_columns = [el for el in all_columns if not el in excluded_headers_list]
    amount = len(filtered_columns)
    
    #Set amt cols and rows to plot
    amt_cols = 3
    amt_rows = math.ceil(amount/float(amt_cols))

    fontsize = 20
    bins = 40
    
    fig, ax_group = plt.subplots(ncols = int(amt_cols), nrows = int(amt_rows), figsize=(20,40))
    for el in range(amount):
        row = el/amt_cols
        col = el-row*amt_cols

        datasets = []

        for df in df_list:
            dataset = df[[filtered_columns[el], 'probability']].copy()
            dataset = dataset.dropna(subset=[filtered_columns[el]])
            datasets.append(dataset)
            
        #Select ax object
        ax_obj = ax_group[row][col]
        
        #Set some layout stuff
        ax_obj.set_ylabel('Probability', fontsize=fontsize)
        ax_obj.set_xlabel(filtered_columns[el], fontsize=fontsize)
        
        entries = []
        
        # Make histograms
        for dataset in datasets:
            entries.append(dataset[filtered_columns[el]].shape[0])
            ax_obj.hist(dataset[filtered_columns[el]], bins=bins, weights=dataset['probability'], histtype='step')

        title_string = filtered_columns[el].replace('_',' ') + " distribution "
        title_string += "\n(n={})".format(str(entries))
        ax_obj.set_title(title_string.title(), fontsize=fontsize)        
            
    for el in range(int(amt_cols * amt_rows - amount)):
        fig.delaxes(ax_group[int(amt_rows-1)][int(amt_cols-1-el)])
    
    #Set general layout stuff
    plt.tight_layout(pad=2.0, w_pad=.5, h_pad=2.0)    
    plt.show()

def auto_plot_all_evolution(df_list, excluded_headers_list, plot_type_dict=None):
    """
    Make the df input so that i'm able to input more dfs, in order to overlay plots automatically
    """
    
    all_columns = df_list[0].columns.values.tolist()
    filtered_columns = [el for el in all_columns if not el in excluded_headers_list]
    amount = len(filtered_columns)

    #Set amt cols and rows to plot
    amt_cols = 3
    amt_rows = math.ceil(amount/float(amt_cols))
    fontsize = 20
    fig, ax_group = plt.subplots(ncols = int(amt_cols), nrows = int(amt_rows), figsize=(20,40))
    for i, ax_obj in enumerate(ax_group.flat):
        if i < amount:
            # Get data from different dataframes
            datasets = []
            for df in df_list:
                dataset = df[[filtered_columns[i], 'time']].copy()
                dataset = dataset.dropna(subset=[filtered_columns[i]])
                datasets.append(dataset)

            #Set some layout stuff
            ax_obj.set_ylabel(str(filtered_columns[i].replace('_',' ')).title(), fontsize=fontsize)
            ax_obj.set_xlabel('Time', fontsize=fontsize)

            entries = []

            # Make histograms
            for dataset in datasets:
                entries.append(dataset[filtered_columns[i]].shape[0])
                ax_obj.scatter(dataset['time'], dataset[filtered_columns[i]])
                if filtered_columns[i] == 't_eff':
                    ax_obj.set_yscale('log')

            title_string = filtered_columns[i].replace('_',' ') + " distribution "
            title_string += "\n(n={})".format(str(entries))
            ax_obj.set_title(title_string.title(), fontsize=fontsize)        
    
    # Delete extra subplots
    for el in range(int(amt_cols * amt_rows - amount)):
        fig.delaxes(ax_group[int(amt_rows-1)][int(amt_cols-1-el)])
    
    # Set general layout stuff
    plt.tight_layout(pad=2.0, w_pad=.5, h_pad=2.0)    
    plt.show()

def auto_plot_all_evolution_include(df_list, include_headers_list, plot_type_dict=None):
    """
    Make the df input so that i'm able to input more dfs, in order to overlay plots automatically
    """
    
    all_columns = df_list[0].columns.values.tolist()
    filtered_columns = include_headers_list
    
    amount = len(filtered_columns)

    #Set amt cols and rows to plot
    amt_cols = 3
    amt_rows = math.ceil(amount/float(amt_cols))
    fontsize = 20
    fig, ax_group = plt.subplots(ncols = int(amt_cols), nrows = int(amt_rows), figsize=(20,40))
    for i, ax_obj in enumerate(ax_group.flat):
        if i < amount:
            # Get data from different dataframes
            datasets = []
            for df in df_list:
                dataset = df[[filtered_columns[i], 'time']].copy()
                dataset = dataset.dropna(subset=[filtered_columns[i]])
                datasets.append(dataset)

            #Set some layout stuff
            ax_obj.set_ylabel(str(filtered_columns[i].replace('_',' ')).title(), fontsize=fontsize)
            ax_obj.set_xlabel('Time', fontsize=fontsize)

            entries = []

            # Make histograms
            for dataset in datasets:
                entries.append(dataset[filtered_columns[i]].shape[0])
                ax_obj.scatter(dataset['time'], dataset[filtered_columns[i]])
                if filtered_columns[i] == 't_eff':
                    ax_obj.set_yscale('log')

            title_string = filtered_columns[i].replace('_',' ') + " distribution "
            title_string += "\n(n={})".format(str(entries))
            ax_obj.set_title(title_string.title(), fontsize=fontsize)        
    
    # Delete extra subplots
    for el in range(int(amt_cols * amt_rows - amount)):
        fig.delaxes(ax_group[int(amt_rows-1)][int(amt_cols-1-el)])
    
    # Set general layout stuff
    plt.tight_layout(pad=2.0, w_pad=.5, h_pad=2.0)    
    plt.show()



def compare_density_four(df_1, df_2, df_3, df_4,
                         param_1, param_2, 
                         min_x, max_x, min_y, max_y, bins, 
                         label1, label2, label3, label4, lower_only=False):
    fig, ax = plt.subplots(nrows=2, ncols=2, figsize=(15,15), sharey=True, sharex=False)
    fig.subplots_adjust(wspace=0)
    fig.subplots_adjust(hspace=0)
    
    #hier een df swap maken ipv een matrix swap. 
    if lower_only:
        df_1[[param_1, param_2]] = df_1[[param_2, param_1]].where(df_1[param_2] > df_1[param_1], df_1[[param_1, param_2]].values)
        df_2[[param_1, param_2]] = df_2[[param_2, param_1]].where(df_2[param_2] > df_2[param_1], df_2[[param_1, param_2]].values)
        df_3[[param_1, param_2]] = df_3[[param_2, param_1]].where(df_3[param_2] > df_3[param_1], df_3[[param_1, param_2]].values)
        df_4[[param_1, param_2]] = df_4[[param_2, param_1]].where(df_4[param_2] > df_4[param_1], df_4[[param_1, param_2]].values)

    X1, Y1, mat1 = make2Dmap_new(
            x=df_1[param_1], 
            y=df_1[param_2], 
            Prob=df_1['probability'],
            minx=min_x, maxx=max_x, miny=min_y, maxy=max_y, bins=bins)
    X2, Y2, mat2 = make2Dmap_new(
            x=df_2[param_1], 
            y=df_2[param_2], 
            Prob=df_2['probability'],
            minx=min_x, maxx=max_x, miny=min_y, maxy=max_y, bins=bins)
    X3, Y3, mat3 = make2Dmap_new(
            x=df_3[param_1], 
            y=df_3[param_2], 
            Prob=df_3['probability'],
            minx=min_x, maxx=max_x, miny=min_y, maxy=max_y, bins=bins)
    X4, Y4, mat4 = make2Dmap_new(
            x=df_4[param_1], 
            y=df_4[param_2], 
            Prob=df_4['probability'],
            minx=min_x, maxx=max_x, miny=min_y, maxy=max_y, bins=bins)
    
    minimal_value1 = np.min(mat1[np.nonzero(mat1)]) #excluding zeros
    minimal_value2 = np.min(mat2[np.nonzero(mat2)]) #excluding zeros
    minimal_value3 = np.min(mat3[np.nonzero(mat3)]) #excluding zeros
    minimal_value4 = np.min(mat4[np.nonzero(mat4)]) #excluding zeros

    max_all= max([mat1.max(), mat2.max(), mat3.max(), mat4.max()])
    min_all= min([minimal_value1, minimal_value2, minimal_value3, minimal_value4])
    
    ax[0][0].pcolor(X1, Y1, mat1, norm=LogNorm(vmin=min_all, vmax=max_all), cmap=CMAP, label=label1)
    ax[0][1].pcolor(X2, Y2, mat2, norm=LogNorm(vmin=min_all, vmax=max_all), cmap=CMAP, label=label2)
    ax[1][0].pcolor(X3, Y3, mat3, norm=LogNorm(vmin=min_all, vmax=max_all), cmap=CMAP, label=label3)
    ax[1][1].pcolor(X4, Y4, mat4, norm=LogNorm(vmin=min_all, vmax=max_all), cmap=CMAP, label=label4)

    ax[0][0].set_xlim(min_x, max_x)
    ax[0][0].set_ylim(min_y, max_y)
    
    ax[0][1].set_xlim(min_x, max_x)
    ax[0][1].set_ylim(min_y, max_y)
    
    ax[1][0].set_xlim(min_x, max_x)
    ax[1][0].set_ylim(min_y, max_y)
    
    ax[1][1].set_xlim(min_x, max_x)
    ax[1][1].set_ylim(min_y, max_y)
    
    return ax

def add_obs(ax):
    ax.errorbar(bhbh_gw_detections_df['mass_1'], bhbh_gw_detections_df['mass_2'], 
                xerr=[bhbh_gw_detections_df['mass_1_upper_error'], bhbh_gw_detections_df['mass_1_lower_error']],
                yerr=[bhbh_gw_detections_df['mass_2_upper_error'], bhbh_gw_detections_df['mass_2_lower_error']], 
             c='r', fmt='o', ecolor='r', capthick=2, linewidth=2, label='Observations')
    return ax

def add_obs_new(ax, par_1, par_2):
    for par in [par_1, par_2]:
        if not par in ['mass_1', 'mass_2', 'mass_chirp']:
            print("WRONG INPUT")
            return None
    ax.errorbar(bhbh_gw_detections_df[par_1], bhbh_gw_detections_df[par_2], 
                xerr=[bhbh_gw_detections_df[par_1+'_upper_error'], bhbh_gw_detections_df['mass_1_lower_error']],
                yerr=[bhbh_gw_detections_df['mass_2_upper_error'], bhbh_gw_detections_df['mass_2_lower_error']], 
             c='r', fmt='o', ecolor='r', capthick=2, linewidth=2, label='Observations')
    return ax

def compare_density(df_1, df_2, param_1, param_2, min_x, max_x, min_y, max_y, 
                    bins, label1, label2, lower_only=False):
    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(15,8), sharey=True)
    fig.subplots_adjust(wspace=0)

    #hier een df swap maken ipv een matrix swap. 
    if lower_only:
        df_1[[param_1, param_2]] = df_1[[param_2, param_1]].where(df_1[param_2] > df_1[param_1], df_1[[param_1, param_2]].values)
        df_2[[param_1, param_2]] = df_2[[param_2, param_1]].where(df_2[param_2] > df_2[param_1], df_2[[param_1, param_2]].values)
    
    X1, Y1, mat1 = make2Dmap_new(
            x=df_1[param_1], 
            y=df_1[param_2], 
            Prob=df_1['probability'],
            minx=min_x, maxx=max_x, miny=min_y, maxy=max_y, bins=bins)
    X2, Y2, mat2 = make2Dmap_new(
            x=df_2[param_1], 
            y=df_2[param_2], 
            Prob=df_2['probability'],
            minx=min_x, maxx=max_x, miny=min_y, maxy=max_y, bins=bins)

#     if lower_only:
#         mat1 = lower_diag_matrix(mat1)       
#         mat2 = lower_diag_matrix(mat2)
    
    minimal_value1 = np.min(mat1[np.nonzero(mat1)]) #excluding zeros
    minimal_value2 = np.min(mat2[np.nonzero(mat2)]) #excluding zeros
    
    max_both= max(mat1.max(), mat2.max())
    min_both= min(minimal_value1, minimal_value2)
    
    ax[0].pcolor(X1, Y1, mat1, norm=LogNorm(vmin=min_both, vmax=max_both), cmap=CMAP, label=label1)
    ax[1].pcolor(X2, Y2, mat2, norm=LogNorm(vmin=min_both, vmax=max_both), cmap=CMAP, label=label2)
    
    ax[0].set_xlim(min_x, max_x)
    ax[0].set_ylim(min_y, max_y)
    
    ax[1].set_xlim(min_x, max_x)
    ax[1].set_ylim(min_y, max_y)
    
    return ax

def make_bars_plot(ax_obj, bars):
    """
    Generate the ax object bars based on the unique stellar types in a dataframe
    """
    bars_stellar_types = sorted([el for el in bars.keys() if not el == 'bins'])
    for i, st_nr in enumerate(bars_stellar_types):
        if not i == 0:
            bottom_list = [np.array(bars[el]) for el in bars_stellar_types[:i]]
            summed_bottom_list = sum(bottom_list)
            ax_obj.bar(bars['bins'][:-1], bars[st_nr], 
                width=np.diff(bars['bins']), bottom=summed_bottom_list, label=STELLAR_TYPE_DICT[bars_stellar_types[i]])
        else:
            ax_obj.bar(bars['bins'][:-1], bars[st_nr], width=np.diff(bars['bins']), label=STELLAR_TYPE_DICT[bars_stellar_types[i]])
    ax_obj.set_ylim(0,1)
    ax_obj.set_ylabel(r'Ratio of remnants in bin')
    return ax_obj
