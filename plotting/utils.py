"""
Script containing useful plot functions
"""

import os
import time
import inspect

import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms


mpl.rc("text", usetex=False)


def add_plot_info(fig, plot_settings):
    """
    Function to add the info at the top of the plot
    """

    ###############
    # Standard plotting adjust
    fig.subplots_adjust(
        top=plot_settings.get("top", 0.85),
        hspace=plot_settings.get("hspace", 1),
    )

    shift = plot_settings.get("shift", -0.05)
    fontsize = plot_settings.get("fontsize", 14)

    # Add info to the plot
    plt.figtext(
        0.5,
        0.925 - shift,
        "Simulation name: {}".format(plot_settings.get("simulation_name", "")),
        fontsize=fontsize,
        ha="center",
    )
    plt.figtext(
        0.5,
        0.9 - shift,
        "Generated on: {}".format(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())),
        fontsize=fontsize,
        ha="center",
    )
    plt.figtext(
        0.5,
        0.875 - shift,
        "Runname: {}".format(plot_settings.get("runname", "")),
        fontsize=fontsize,
        ha="center",
    )

    return fig


def show_and_save_plot(fig, plot_settings, verbose=1):
    """
    Wrapper to handle the saving and the showing of the plots
    """

    if plot_settings.get("show_plot", False):
        plt.show(block=plot_settings.get("block", True))
    else:
        plt.ioff()

    if plot_settings.get("output_name", None):
        if os.path.dirname(plot_settings["output_name"]):
            if verbose:
                print(
                    "creating output directory {}".format(
                        os.path.dirname(plot_settings["output_name"])
                    )
                )
            os.makedirs(os.path.dirname(plot_settings["output_name"]), exist_ok=True)
        fig.savefig(plot_settings["output_name"], bbox_inches="tight")

        try:
            caller_name = inspect.currentframe().f_back.f_code.co_name
        except:
            caller_name = ""
        if verbose:
            print(
                "{}wrote plot to {}".format(
                    "{}: ".format(caller_name) if caller_name else "",
                    plot_settings["output_name"],
                )
            )

    # Stuff to close and handle the cleaning of the garbage
    plt.close(fig)
    plt.close("all")


def save_loop(name, formats, **kwargs):
    """
    Functon to save plots to location in a loop with chosen set of formats
    beware to make the name include ".{format}" at the end.
    """

    if os.path.dirname(name):
        os.makedirs(os.path.dirname(name), exist_ok=True)

    for format_type in formats:
        plt.savefig(name.format(format=format_type), **kwargs)


def align_axes(fig, axes_list, which_axis="x"):
    """
    Function to align the x or y axis of a list of axes
    """

    if which_axis == "x":
        getter = "get_xlim"
        setter = "set_xlim"
        log_getter = "get_xscale"
        log_setter = "set_xscale"
    elif which_axis == "y":
        getter = "get_ylim"
        setter = "set_ylim"
        log_getter = "get_yscale"
        log_setter = "set_yscale"
    else:
        raise ValueError("not implemented yet")

    min_val = 1e9
    max_val = 1e-9    

    #
    any_log = False
    for axis in axes_list:
        logscale = axis.__getattribute__(log_getter)()
        if logscale == 'log':
            any_log = True

    # Find the min and max
    for axis in axes_list:
        lims = axis.__getattribute__(getter)()

        min_val = np.min([min_val, lims[0]])
        max_val = np.max([max_val, lims[1]])

    # Set the min and max
    for axis in axes_list:
        print(axis)
        axis.__getattribute__(setter)([min_val, max_val])


    # handle setting to log if required
    if any_log:
        for axis in axes_list:
            logscale = axis.__getattribute__(log_setter)("log")







def align_axes_with(fig, axis, other_axes_list, which_axis="x"):
    """
    Function to align a list of other axes with a specific axis
    """

    if which_axis == "x":
        getter = "get_xlim"
        setter = "set_xlim"
        log_getter = "get_xscale"
        log_setter = "set_xscale"
    elif which_axis == "y":
        getter = "get_ylim"
        setter = "set_ylim"
        log_getter = "get_yscale"
        log_setter = "set_yscale"
    else:
        raise ValueError("not implemented yet")

    min_val = 1e9
    max_val = 1e-9    

    # check if logscale
    logscale = axis.__getattribute__(log_getter)()
    if logscale == 'log':
        any_log = True


    # Find lims
    lims = axis.__getattribute__(getter)()

    min_val = np.min([min_val, lims[0]])
    max_val = np.max([max_val, lims[1]])

    # Set the min and max
    for axis in other_axes_list:
        axis.__getattribute__(setter)([min_val, max_val])


    # handle setting to log if required
    if any_log:
        for axis in other_axes_list:
            logscale = axis.__getattribute__(log_setter)("log")


def align_both_axes(fig, axes_list):
    """
    Function to align both axes
    """

    align_axes(fig, axes_list, which_axis="x")
    align_axes(fig, axes_list, which_axis="y")

def delete_out_of_frame_text(fig, ax):
    """
    Function that deletes text objects that fall out of frame
    """

    # find text out of bounds
    xlims = ax.get_xlim()
    ylims = ax.get_ylim()

    for text in ax.texts:
        text_pos = text.get_position()
        if (not xlims[0] <= text_pos[0] <= xlims[1]) or (not ylims[0] <= text_pos[1] <= ylims[1]):
            text.remove()

    return fig, ax


def add_labels_subplots(fig, axes_list, label_function_kwargs, custom_locs=None):
    """
    Function to loop over a list of axes of a figure and add subplots
    """

    # Add subplot labels
    for ax_i, ax in enumerate(axes_list):
        panel_label_function_kwargs = label_function_kwargs
        # Custom per-panel placement
        if custom_locs is not None:
            panel_label_function_kwargs = {**panel_label_function_kwargs, 'x_loc': custom_locs[ax_i]['x'], 'y_loc': custom_locs[ax_i]['y'],}

        add_label_subplot(fig=fig, ax=ax, label_i=ax_i, **panel_label_function_kwargs)

def add_label_subplot(
    fig,
    ax,
    label_i,
    x_loc=0.95,
    y_loc=0.95,
    fontsize=24,
    verticalalignment="top",
    bbox_dict={"pad": 0.5, "facecolor": "None", "boxstyle": "round"},
):
    """
    Function to add a label to the subplot
    """

    subplot_labels = "abcdefghijklmnopqrstuvwxyz"

    trans = mtransforms.ScaledTranslation(10 / 72, -5 / 72, fig.dpi_scale_trans)
    ax.text(
        x_loc,
        y_loc,
        subplot_labels[label_i],
        transform=ax.transAxes + trans,
        fontsize=fontsize,
        verticalalignment=verticalalignment,
        bbox=dict(**bbox_dict),
        zorder=500
    )

    return fig, ax
