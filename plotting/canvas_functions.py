"""
Function to design the layout of the canvas for the comparison of the queries
"""

import matplotlib.pyplot as plt

# general cornerplot
def general_corner_plot_with_colorbar(n_parameters):
    """
    Function to set up a figure with an N parameter corner plot, including a colorbar
    """

    fig = plt.figure(figsize=(30, 30))

    scale = 3

    gs = fig.add_gridspec(nrows=n_parameters * scale, ncols=(n_parameters * scale) + 2)

    # Create empty numpy array
    axis_matrix = [[None for _ in range(n_parameters)] for _ in range(n_parameters)]

    # Create diagonal
    diagonal_axes = {}
    for n_parameter_i in range(n_parameters):
        axis = fig.add_subplot(gs[n_parameter_i*scale:(n_parameter_i+1)*scale, n_parameter_i*scale:(n_parameter_i+1)*scale])
        diagonal_axes[n_parameter_i] = axis

        axis_matrix[n_parameter_i][n_parameter_i] = axis

    # Create off-diagonal
    for i in range(n_parameters-1):
        for j in range(i+1, n_parameters):
            axis = fig.add_subplot(gs[j*scale:(j+1)*scale, i*scale:(i+1)*scale])
            axis_matrix[j][i] = axis

    # Fix labels
    for i in range(n_parameters):
        for j in range(n_parameters):
            # Column:
            if j == 0:
                if not i == n_parameters-1:
                    axis_matrix[i][j].set_xticklabels([])
            elif i == n_parameters-1:
                if not j == 0:
                    axis_matrix[i][j].set_yticklabels([])
            else:
                if axis_matrix[i][j]:
                    axis_matrix[i][j].set_xticklabels([])
                    axis_matrix[i][j].set_yticklabels([])

    colorbar_axis = fig.add_subplot(gs[:, -1])

    return fig, diagonal_axes, colorbar_axis, axis_matrix

def return_canvas_with_0_subsets(fig=None):
    """
    Function to return the canvas for 3 subsets
    """

    if fig is None:
        fig = plt.figure(figsize=(50, 40))

    gs = fig.add_gridspec(nrows=1, ncols=10)

    # Create axes
    axes_all = fig.add_subplot(gs[:, :-2])

    #
    axes_colorbar = fig.add_subplot(gs[:, -1])

    # Set up the dict that contains all the axes info
    axes_dict = {
        'all_axis': axes_all,
        'colorbar_axis': axes_colorbar,
    }

    return fig, gs, axes_dict


def return_canvas_with_2_subsets(fig=None, add_ratio_axes=False):
    """
    Function to return the canvas for 2 subsets
    """

    if not add_ratio_axes:
        if fig is None:
            fig = plt.figure(figsize=(80, 40))

        gs = fig.add_gridspec(nrows=4, ncols=10)

        # Create axes
        axes_all = fig.add_subplot(gs[:2, 1:7])

        axes_query1 = fig.add_subplot(gs[2:, :4])
        axes_query2 = fig.add_subplot(gs[2:, 4:8])

        #
        axes_colorbar = fig.add_subplot(gs[:, -1])

        # invisible axis
        axes_invisible = fig.add_subplot(gs[:, :-2], frame_on=False)
        axes_invisible.set_xticks([])
        axes_invisible.set_yticks([])

        # Remove axes labels etc
        axes_query2.set_yticklabels([])

        # Set up the dict that contains all the axes info
        axes_dict = {
            'all_axis': axes_all,
            'subset_axes': [axes_query1, axes_query2],
            'colorbar_axis': axes_colorbar,
            'invisible_axis': axes_invisible
        }

    else:
        if fig is None:
            fig = plt.figure(figsize=(80, 60))

        gs = fig.add_gridspec(nrows=6, ncols=10)

        # Create axes
        axes_all = fig.add_subplot(gs[:2, 1:7])

        axes_query1 = fig.add_subplot(gs[2:4, :4])
        axes_query2 = fig.add_subplot(gs[2:4, 4:8])

        axes_ratio1 = fig.add_subplot(gs[4:, :4])
        axes_ratio2 = fig.add_subplot(gs[4:, 4:8])

        #
        axes_colorbar = fig.add_subplot(gs[:4, -1])
        axes_colorbar_ratio = fig.add_subplot(gs[4:, -1])

        # invisible axis
        axes_invisible = fig.add_subplot(gs[:, :-2], frame_on=False)
        axes_invisible.set_xticks([])
        axes_invisible.set_yticks([])

        # Remove axes labels etc
        axes_query2.set_yticklabels([])
        axes_ratio2.set_yticklabels([])

        # Set up the dict that contains all the axes info
        axes_dict = {
            'all_axis': axes_all,
            'subset_axes': [axes_query1, axes_query2],
            'ratio_axes': [axes_ratio1, axes_ratio2],
            'colorbar_axis': axes_colorbar,
            'colorbar_axis_ratio': axes_colorbar_ratio,
            'invisible_axis': axes_invisible,
        }

    return fig, gs, axes_dict

def return_canvas_with_3_subsets(add_ratio_axes=False, fig=None):
    """
    Function to return the canvas for 3 subsets
    """

    if not add_ratio_axes:
        if fig is None:
            fig = plt.figure(figsize=(80, 40))

        gs = fig.add_gridspec(nrows=2, ncols=11)

        # Create axes
        axes_all = fig.add_subplot(gs[0, 3:6])

        axes_query1 = fig.add_subplot(gs[1, :3])
        axes_query2 = fig.add_subplot(gs[1:, 3:6])
        axes_query3 = fig.add_subplot(gs[1:, 6:9])

        axes_colorbar = fig.add_subplot(gs[:, -1])

        # Remove axes labels etc
        axes_query2.set_yticklabels([])
        axes_query3.set_yticklabels([])

        # invisible axis
        axes_invisible = fig.add_subplot(gs[:, :-2], frame_on=False)
        axes_invisible.set_xticks([])
        axes_invisible.set_yticks([])

        # Set up the dict that contains all the axes info
        axes_dict = {
            'all_axis': axes_all,
            'subset_axes': [axes_query1, axes_query2, axes_query3],
            'colorbar_axis': axes_colorbar,
            'invisible_axis': axes_invisible
        }

    else:
        if fig is None:
            fig = plt.figure(figsize=(80, 60))

        gs = fig.add_gridspec(nrows=3, ncols=11)

        # Create axes
        axes_all = fig.add_subplot(gs[0, 2:7])

        axes_query1 = fig.add_subplot(gs[1, :3])
        axes_query2 = fig.add_subplot(gs[1, 3:6])
        axes_query3 = fig.add_subplot(gs[1, 6:9])

        axes_colorbar = fig.add_subplot(gs[:2, -1])


        axes_ratio1 = fig.add_subplot(gs[2, :3])
        axes_ratio2 = fig.add_subplot(gs[2, 3:6])
        axes_ratio3 = fig.add_subplot(gs[2, 6:9])

        axes_colorbar_ratio = fig.add_subplot(gs[2, -1])

        # Remove axes labels etc
        axes_query2.set_yticklabels([])
        axes_query3.set_yticklabels([])

        axes_ratio2.set_yticklabels([])
        axes_ratio3.set_yticklabels([])

        # invisible axis
        axes_invisible = fig.add_subplot(gs[:, :-2], frame_on=False)
        axes_invisible.set_xticks([])
        axes_invisible.set_yticks([])

        # Set up the dict that contains all the axes info
        axes_dict = {
            'all_axis': axes_all,
            'subset_axes': [axes_query1, axes_query2, axes_query3],
            'ratio_axes': [axes_ratio1, axes_ratio2, axes_ratio3],
            'colorbar_axis': axes_colorbar,
            'colorbar_axis_ratio': axes_colorbar_ratio,
            'invisible_axis': axes_invisible
        }

    return fig, gs, axes_dict

def return_canvas_with_4_subsets(fig):
    """
    Function to return the canvas for 3 subsets
    """

    gs = fig.add_gridspec(nrows=3, ncols=6)

    # Create axes
    axes_all = fig.add_subplot(gs[0, 1:3])

    axes_query1 = fig.add_subplot(gs[1, :2])
    axes_query2 = fig.add_subplot(gs[1, 2:4])

    axes_query3 = fig.add_subplot(gs[2, :2])
    axes_query4 = fig.add_subplot(gs[2, 2:4])

    axes_colorbar = fig.add_subplot(gs[:, -1])

    # Remove axes labels etc
    axes_query2.set_yticklabels([])
    axes_query4.set_yticklabels([])

    # Remove axes labels etc
    axes_query1.set_xticklabels([])
    axes_query2.set_xticklabels([])

    #
    axes_query3.xaxis.get_major_ticks()[-1].label1.set_visible(False) ## set last x tick label invisible

    # invisible axis
    axes_invisible = fig.add_subplot(gs[:, :-2], frame_on=False)
    axes_invisible.set_xticks([])
    axes_invisible.set_yticks([])

    # Set up the dict that contains all the axes info
    axes_dict = {
        'all_axis': axes_all,
        'subset_axes': [axes_query1, axes_query2, axes_query3, axes_query4],
        'colorbar_axis': axes_colorbar,
        'invisible_axis': axes_invisible
    }

    return fig, gs, axes_dict

def return_canvas_with_subsets(amt_subsets, fig=None, add_ratio_axes=False):
    """
    Function to return a canvas with space for all the subsets.
    """

    #
    if amt_subsets==0:
        return return_canvas_with_0_subsets(fig=fig)
    elif amt_subsets==2:
        return return_canvas_with_2_subsets(fig=fig, add_ratio_axes=add_ratio_axes)
    elif amt_subsets==3:
        return return_canvas_with_3_subsets(fig=fig, add_ratio_axes=add_ratio_axes)
    elif amt_subsets==4:
        return return_canvas_with_4_subsets(fig=fig, add_ratio_axes=add_ratio_axes)

def return_canvas_with_n_squares(n_keys):
    """
    function to return a canvas with n squares on a 2x(n/2) grid
    """

    #
    square_size = 6

    #
    fig, axes = plt.subplots(
        nrows=math.ceil((n_keys/2)),
        ncols=int(2),
        figsize=(square_size * 2, square_size * (n_keys/2))
    )

    return fig, axes
