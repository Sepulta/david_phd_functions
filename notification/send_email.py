"""
Script to send an email when a function or simulation is finished.

https://leimao.github.io/blog/Python-Send-Gmail/
- I make use of the 'new authentication - less secure: app password' method

NOTE: use a environment variable named 'SIMULATION_NOTIFICATION_APP_PW' to store your app password
NOTE: change CORRESPONDING EMAIL to your email
"""

import os
import traceback
import datetime
import smtplib, ssl
from email.message import EmailMessage


CORRESPONDING_EMAIL = "davidhendriks93@gmail.com"


def send_finished_email(simulation_name, status, extra_message=""):
    """
    Function to send an email to
    """

    # configuration
    port = 465  # For SSL
    smtp_server = "smtp.gmail.com"
    sender_email = CORRESPONDING_EMAIL  # Enter your address
    receiver_email = CORRESPONDING_EMAIL  # Enter receiver address
    password = os.getenv('SIMULATION_NOTIFICATION_APP_PW')

    # set content
    datetimestring = datetime.datetime.utcnow().isoformat()
    subject = "Simulaton {simulation_name} status {status}".format(simulation_name=simulation_name, status=status)
    body_text = """
Simulaton {simulation_name} finished on {time} with the following status {status}.{extra_message_part}
""".format(
        simulation_name=simulation_name,
        time=datetimestring,
        status=status,
        extra_message_part="\n\n{}".format(extra_message) if extra_message else ""
    ).strip()

    # Set up email object
    msg = EmailMessage()
    msg.set_content(body_text)
    msg['Subject'] = subject
    msg['From'] = sender_email
    msg['To'] = receiver_email


    # Send email
    try:
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
            server.login(sender_email, password)
            server.send_message(msg, from_addr=sender_email, to_addrs=receiver_email)
    except BaseException as error:
            print("An exception occurred: {}".format(exception))
            print(f"Exception Name: {type(exception).__name__}")
            print(f"Exception Desc: {exception}")
            print(f"Exception traceback: {traceback.print_exc()}")


if __name__=="__main__":
    simulation_name='testing_simname'
    status='successful'

    #
    send_finished_email(
        simulation_name=simulation_name,
        status=status
    )


    simulation_name='testing_simname'
    status='failed'
    extra_message = "some backtrace message with error"

    #
    send_finished_email(
        simulation_name=simulation_name,
        status=status,
        extra_message=extra_message
    )