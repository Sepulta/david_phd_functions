import git
import datetime

def get_git_info_and_time():
    """
    Function to get current git info
    """

    # Get repo
    repo = git.Repo(search_parent_directories=True)

    # get repo name
    repo_name = repo.remotes.origin.url.split(".git")[0].split("/")[-1]

    # Get branch
    branch = repo.active_branch
    branch = branch.name

    # get head commit
    sha = repo.head.object.hexsha

    iso_date = datetime.datetime.now().isoformat("#")

    # Set up return dict
    return_dict = {
        "datetime_string": iso_date,
        "repo_name": repo_name,
        "branch_name": branch,
        "commit_sha": sha,
    }

    return return_dict